<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@loginPage');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'authenticated'], function () {
    Route::get('/', 'HomeController@index');

    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::group(['prefix' => 'workzone'], function () {
            Route::get('/', 'WorkzoneController@index')
                ->middleware('hasPermission:auth.workzone,read');

            Route::get('api/all', 'WorkzoneController@apiAll')
                ->middleware('hasPermission:auth.workzone,read');

            Route::post('api/create', 'WorkzoneController@apiCreate')
                ->middleware('hasPermission:auth.workzone,write');

            Route::post('api/update', 'WorkzoneController@apiUpdate')
                ->middleware('hasPermission:auth.workzone,write');
        });

        Route::group(['prefix' => 'permission'], function () {
            Route::get('/', 'PermissionController@index')
                ->middleware('hasPermission:auth.permission,read');

            Route::post('/', 'PermissionController@create')
                ->middleware('hasPermission:auth.permission,write');

            Route::put('/', 'PermissionController@update')
                ->middleware('hasPermission:auth.permission,write');
        });

        Route::group(['prefix' => 'user'], function () {
            Route::get('/', 'UserController@index')
                ->middleware('hasPermission:auth.user,read');

            Route::get('/new', 'UserController@createForm')
                ->middleware('hasPermission:auth.user,write');

            Route::post('/new', 'UserController@create')
                ->middleware('hasPermission:auth.user,write');

            Route::get('/{id}', 'UserController@updateForm')
                ->middleware('hasPermission:auth.user,read');

            Route::post('/{id}', 'UserController@update')
                ->middleware('hasPermission:auth.user,write');
        });
    });

    Route::group(['prefix' => 'mcore', 'namespace' => 'Mcore'], function () {
        Route::get('/', 'MapController@mapView');

        Route::group(['prefix' => 'sto'], function () {
            Route::get('/', 'StoController@index')
                ->middleware('hasPermission:mcore.sto,read');

            Route::get('/workzone/{id}.select2', 'StoController@listByWorkzoneAsSelect2')
                ->middleware('hasPermission:mcore.sto,read');

            Route::get('/{id}/room.select2', 'StoController@listRoomByStoAsSelect2')
                ->middleware('hasPermission:mcore.sto,read');

            Route::get('/room/{id}/odf.select2', 'StoRoomController@listOdfByRoomAsSelect2')
                ->middleware('hasPermission:mcore.sto.odf,read');

            Route::get('/room/odf/{id}/otb.select2', 'OdfController@listPanelByOdfAsSelect2')
                ->middleware('hasPermission:mcore.sto.odf,read');

            Route::get('/room/odf/otb/{id}/port.select2', 'OdfPanelController@listPortByPanelAsSelect2')
                ->middleware('hasPermission:mcore.sto.odf,read');

            Route::get('/workzone/{id}', 'StoController@listByWorkzone')
                ->middleware('hasPermission:mcore.sto,read');

            Route::get('/new', 'StoController@createForm')
                ->middleware('hasPermission:mcore.sto,write');

            Route::post('/new', 'StoController@create')
                ->middleware('hasPermission:mcore.sto,write');

            Route::get('/{id}', 'StoController@updateForm')
                ->middleware('hasPermission:mcore.sto,read');

            Route::post('/{id}', 'StoController@update')
                ->middleware('hasPermission:mcore.sto,write');

            Route::get('/{id}/history', 'StoController@historyList')
                ->middleware('hasPermission:mcore.sto,read_audit');

            Route::post('/{id}/newroom', 'StoRoomController@create')
                ->middleware('hasPermission:mcore.sto,write');

            Route::get('/{stoId}/room/{roomId}', 'StoRoomController@view')
                ->middleware('hasPermission:mcore.sto,read');

            Route::post('/{stoId}/room/{roomId}', 'StoRoomController@update')
                ->middleware('hasPermission:mcore.sto,write');


            Route::group(['prefix' => '/{stoId}/room/{roomId}/olt'], function () {
                Route::get('/new', 'OltController@createForm')
                    ->middleware('hasPermission:mcore.sto.olt,write');

                Route::post('/new', 'OltController@create')
                    ->middleware('hasPermission:mcore.sto.olt,write');
            });


            Route::group(['prefix' => '/{stoId}/room/{roomId}/odf'], function () {
                Route::get('/new', 'OdfController@createForm')
                    ->middleware('hasPermission:mcore.sto.odf,write');

                Route::post('/new', 'OdfController@create')
                    ->middleware('hasPermission:mcore.sto.odf,write');

                Route::get('/{odfId}', 'OdfController@updateForm')
                    ->middleware('hasPermission:mcore.sto.odf,read');

                Route::post('/{odfId}', 'OdfController@update')
                    ->middleware('hasPermission:mcore.sto.odf,write');

                Route::group(['prefix' => '/{odfId}/panel'], function () {
                    Route::get('{panelId}/port/{port}', 'OdfPanelController@rearLink');

                    Route::get('/new', 'OdfPanelController@createForm')
                        ->middleware('hasPermission:mcore.sto.odf.panel,write');

                    Route::post('/new', 'OdfPanelController@create')
                        ->middleware('hasPermission:mcore.sto.odf.panel,write');

                    Route::get('/{panelId}', 'OdfPanelController@updateForm')
                        ->middleware('hasPermission:mcore.sto.odf.panel,read');

                    Route::post('/{panelId}', 'OdfPanelController@update')
                        ->middleware('hasPermission:mcore.sto.odf.panel,write');
                });
            });

        });

        Route::group(['prefix' => 'odc'], function () {
            Route::get('/', 'OdcController@index')
                ->middleware('hasPermission:mcore.odc,read');

            Route::get('/workzone/{id}.select2', 'OdcController@listByWorkzoneAsSelect2')
                ->middleware('hasPermission:mcore.odc,read');

            Route::get('/workzone/{id}', 'OdcController@listByWorkzone')
                ->middleware('hasPermission:mcore.odc,read');

            Route::get('/new', 'OdcController@createForm')
                ->middleware('hasPermission:mcore.odc,write');

            Route::post('/new', 'OdcController@create')
                ->middleware('hasPermission:mcore.odc,write');

            Route::get('/{id}/rearport.select2', 'OdcController@rearPortAsSelect2')
                ->middleware('hasPermission:mcore.link.odc-odp,read');

            Route::get('/{id}', 'OdcController@updateForm')
                ->middleware('hasPermission:mcore.odc,read');

            Route::post('/{id}', 'OdcController@update')
                ->middleware('hasPermission:mcore.odc,write');

            Route::post('/{id}/splitter', 'OdcController@submitSplitter')
                ->middleware('hasPermission:mcore.odc,write');

            Route::get('/{id}/panel/{panel}/port/{port}', 'OdcController@rearLink');
        });

        Route::group(['prefix' => 'odp'], function () {
            Route::get('/', 'OdpController@index')
                ->middleware('hasPermission:mcore.odp,read');

            Route::get('/workzone/{id}.select2', 'OdpController@listByWorkzoneAsSelect2')
                ->middleware('hasPermission:mcore.odp,read');

            Route::get('/workzone/{id}', 'OdpController@listByWorkzone')
                ->middleware('hasPermission:mcore.odp,read');

            Route::get('/new', 'OdpController@createForm')
                ->middleware('hasPermission:mcore.odp,write');

            Route::post('/new', 'OdpController@create')
                ->middleware('hasPermission:mcore.odp,write');

            Route::get('/{id}', 'OdpController@updateForm')
                ->middleware('hasPermission:mcore.odp,read');

            Route::post('/{id}', 'OdpController@update')
                ->middleware('hasPermission:mcore.odp,write');
        });

        Route::group(['prefix' => 'pelanggan'], function () {
            Route::get('/', 'PelangganController@index')
                ->middleware('hasPermission:mcore.pelanggan,read');

            Route::get('/workzone/{id}', 'PelangganController@listByWorkzone')
                ->middleware('hasPermission:mcore.pelanggan,read');

            Route::get('/new', 'PelangganController@createForm')
                ->middleware('hasPermission:mcore.pelanggan,write');

            Route::post('/new', 'PelangganController@create')
                ->middleware('hasPermission:mcore.pelanggan,write');

            Route::get('/{id}', 'PelangganController@updateForm')
                ->middleware('hasPermission:mcore.pelanggan,read');

            Route::post('/{id}', 'PelangganController@update')
                ->middleware('hasPermission:mcore.pelanggan,write');
        });

        Route::group(['prefix' => 'feeder'], function () {
            Route::get('/', 'FeederController@index')
                ->middleware('hasPermission:mcore.feeder,read');

            Route::get('/workzone/{id}.select2', 'FeederController@listByWorkzoneAsSelect2')
                ->middleware('hasPermission:mcore.feeder,read');

            Route::get('/{id}/core.select2', 'FeederController@coreByIdAsSelect2')
                ->middleware('hasPermission:mcore.feeder,read');

            Route::get('/workzone/{id}', 'FeederController@listByWorkzone')
                ->middleware('hasPermission:mcore.feeder,read');

            Route::get('/new', 'FeederController@createForm')
                ->middleware('hasPermission:mcore.feeder,write');

            Route::post('/new', 'FeederController@create')
                ->middleware('hasPermission:mcore.feeder,write');

            Route::get('/{id}', 'FeederController@show')
                ->middleware('hasPermission:mcore.feeder,read');

            Route::post('/{id}', 'FeederController@update')
                ->middleware('hasPermission:mcore.feeder,write');

            Route::get('/{id}/route', 'FeederController@showRoute')
                ->middleware('hasPermission:mcore.feeder,read');

            Route::post('/{id}/route', 'FeederController@submitRoute')
                ->middleware('hasPermission:mcore.feeder,write');
        });

        Route::group(['prefix' => 'distribusi'], function () {
            Route::get('/', 'DistribusiController@index')
                ->middleware('hasPermission:mcore.distribusi,read');

            Route::get('/workzone/{id}', 'DistribusiController@listByWorkzone')
                ->middleware('hasPermission:mcore.distribusi,read');

            Route::get('/new', 'DistribusiController@createForm')
                ->middleware('hasPermission:mcore.distribusi,write');

            Route::post('/new', 'DistribusiController@create')
                ->middleware('hasPermission:mcore.distribusi,write');

            Route::get('/{id}', 'DistribusiController@show')
                ->middleware('hasPermission:mcore.distribusi,read');

            Route::post('/{id}', 'DistribusiController@update')
                ->middleware('hasPermission:mcore.distribusi,write');

            Route::get('/{id}/route', 'DistribusiController@showRoute')
                ->middleware('hasPermission:mcore.distribusi,read');

            Route::post('/{id}/route', 'DistribusiController@submitRoute')
                ->middleware('hasPermission:mcore.distribusi,write');
        });

        Route::group(['namespace' => 'Link'], function () {
            /*
             * FEEDER (ODF/Panel - ODC)
             */
            Route::get('/odc/{id}/panel/{panel}/port/{port}/odf', 'FeederLinkController@odcForm')
                ->middleware('hasPermission:mcore.link.otb-odc,read');

            Route::post('/odc/{id}/panel/{panel}/port/{port}/odf', 'FeederLinkController@submit')
                ->middleware('hasPermission:mcore.link.otb-odc,write');

            Route::get('/sto/{stoId}/room/{roomId}/odf/{odfId}/panel/{id}/port/{port}/odc', 'FeederLinkController@otbForm')
                ->middleware('hasPermission:mcore.link.otb-odc,read');

            Route::post('/sto/{stoId}/room/{roomId}/odf/{odfId}/panel/{id}/port/{port}/odc', 'FeederLinkController@submit')
                ->middleware('hasPermission:mcore.link.otb-odc,write');

            /*
             * DISTRIBUSI (ODC - ODP)
             */
            Route::get('/distribusi/link/workzone/{id}.select2', 'OdcOdpLinkController@distribusiByWorkzoneAsSelect2')
                ->middleware('hasPermission:mcore.link.odc-odp,read');

            Route::get('/distribusi/{id}/core.select2', 'OdcOdpLinkController@coreByDistribusiAsSelect2')
                ->middleware('hasPermission:mcore.link.odc-odp,read');

            Route::get('/odc/{id}/panel/{panel}/port/{port}/odp', 'OdcOdpLinkController@odcForm')
                ->middleware('hasPermission:mcore.link.odc-odp,read');

            Route::post('/odc/{id}/panel/{panel}/port/{port}/odp', 'OdcOdpLinkController@submit')
                ->middleware('hasPermission:mcore.link.odc-odp,write');

            Route::get('/odp/{id}/dist', 'OdcOdpLinkController@odpForm')
                ->middleware('hasPermission:mcore.link.odc-odp,read');

            Route::post('/odp/{id}/dist', 'OdcOdpLinkController@submit')
                ->middleware('hasPermission:mcore.link.odc-odp,write');

            /*
             * DROP CORE (ODP - ONT)
             */
            Route::get('/odp/workzone/{id}/link.select2', 'OdcOdpLinkController@odpByWorkzoneAsSelect2')
                ->middleware('hasPermission:mcore.odp,read');

            Route::get('/pelanggan/link/workzone/{id}.select2', 'OdpOntLinkController@pelangganByWorkzoneAsSelect2')
                ->middleware('hasPermission:mcore.link.odp-ont,read');

            Route::get('/odp/{id}/ports.select2', 'OdpOntLinkController@odpPortsAsSelect2')
                ->middleware('hasPermission:mcore.link.odp-ont,read');

            Route::get('/odp/{id}/port/{port}', 'OdpOntLinkController@odpForm')
                ->middleware('hasPermission:mcore.link.odp-ont,read');

            Route::post('/odp/{id}/port/{port}', 'OdpOntLinkController@submit')
                ->middleware('hasPermission:mcore.link.odp-ont,write');

            Route::get('/pelanggan/link/{id}', 'OdpOntLinkController@pelangganForm')
                ->middleware('hasPermission:mcore.link.odp-ont,read');

            Route::post('/pelanggan/link/{id}', 'OdpOntLinkController@submit')
                ->middleware('hasPermission:mcore.link.odp-ont,write');

        });

        Route::group(['prefix' => 'map'], function () {
            Route::get('/sto', 'MapController@allSto')
                ->middleware('hasPermission:mcore.sto,read');

            Route::get('/grid/{type}/{zoom}/{x},{y}', 'MapController@grid');

            Route::get('/cellTest/{zoom}/{x},{y}', 'MapController@cellTest');
        });

        Route::group(['prefix' => 'alpro'], function () {
            Route::get('/', 'AlproController@inputForm')
                ->middleware('hasPermission:mcore.alpro,write');

            Route::post('/', 'AlproController@create')
                ->middleware('hasPermission:mcore.alpro,write');
        });
    });
});
