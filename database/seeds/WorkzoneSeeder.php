<?php

use Illuminate\Database\Seeder;
use App\Service\Auth\Workzone;

class WorkzoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r1 = Workzone::create('Regional I');

        $w1_1 = Workzone::create('WITEL 1.1', $r1->path);
        Workzone::create('AREA 1.1.1', $w1_1->path);
        Workzone::create('AREA 1.1.2', $w1_1->path);

        $w1_2 = Workzone::create('WITEL 1.2', $r1->path);
        Workzone::create('AREA 1.2.1', $w1_2->path);
        Workzone::create('AREA 1.2.2', $w1_2->path);


        $r2 = Workzone::create('Regional II');

        $w2_1 = Workzone::create('WITEL 2.1', $r2->path);
        Workzone::create('AREA 2.1.1', $w2_1->path);
        Workzone::create('AREA 2.1.2', $w2_1->path);

        $w2_2 = Workzone::create('WITEL 2.2', $r2->path);
        Workzone::create('AREA 2.2.1', $w2_2->path);
        Workzone::create('AREA 2.2.2', $w2_2->path);
    }
}
