<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Service\Mcore\Sto;
use App\Service\Mcore\StoRoom;
use App\Service\Mcore\Odf;
use App\Service\Mcore\OdfPanel;

class McoreStoSeeder extends Seeder
{
    public function run()
    {
        $wz = DB::table('auth.workzone')
            ->orderBy('id', 'DESC')
            ->select('id')
            ->limit(1)
            ->first()
        ;

        try {
            $stoId = Sto::create(
                1,
                $wz->id,
                'STO-SEED-001',
                0,
                0
            );

            $roomId = StoRoom::create(
                1,
                $stoId,
                'ROOM-SEED-001'
            );

            $odfId = Odf::create(
                1,
                $roomId,
                'ODF-SEED-001',
                'GROUP001'
            );

            OdfPanel::create(
                1,
                $odfId,
                'OTB-SEED-001',
                2,
                12,
                false
            );
        } catch (\Throwable $e) { throw $e; }
    }
}
