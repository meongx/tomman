<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Service\Mcore\Pelanggan;

class McorePelangganSeeder extends Seeder
{
    public function run()
    {
        $wz = DB::table('auth.workzone')
            ->orderBy('id', 'DESC')
            ->select('id')
            ->limit(1)
            ->first()
        ;

        try {
            Pelanggan::create(
                1,
                $wz->id,
                'seed00001',
                'Pelanggan Seed 001',
                0,
                'Person in Charge',
                'Alamat',
                'Keterangan',
                0,
                0
            );

            Pelanggan::create(
                1,
                $wz->id,
                'seed00002',
                'Pelanggan Seed 002',
                0,
                '',
                '',
                '',
                0,
                0
            );
        } catch(\Throwable $e) { throw $e; }
    }
}
