<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreStoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.sto(
              id SERIAL PRIMARY KEY,
              workzone_id SMALLINT REFERENCES auth.workzone(id),
              label TEXT NOT NULL CHECK (label <> ''),
              coordinate GEOMETRY(POINT, 4326)
            )
        ");

        DB::statement("CREATE INDEX ON mcore.sto(workzone_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.sto');
    }
}
