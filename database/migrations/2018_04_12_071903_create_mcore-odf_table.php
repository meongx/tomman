<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreOdfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.odf(
              id BIGSERIAL PRIMARY KEY,
              sto_room_id BIGINT REFERENCES mcore.sto_room(id),
              label TEXT NOT NULL,
              \"group\" TEXT NOT NULL
            )
        ");

        DB::statement("CREATE INDEX ON mcore.odf(sto_room_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.odf');
    }
}
