<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreAlproTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.alpro(
              id BIGSERIAL PRIMARY KEY,
              workzone_id SMALLINT REFERENCES auth.workzone(id),
              type SMALLINT NOT NULL DEFAULT '0',
              coordinate GEOMETRY(POINT, 4326)
            )
        ");

        DB::statement("CREATE INDEX ON mcore.alpro(workzone_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.alpro');
    }
}
