<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAuthWorkzoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE auth.workzone(
              id    SERIAL PRIMARY KEY,
              label  TEXT NOT NULL CHECK (label <> ''),
              path  LTREE
            )
        ");

        DB::statement("CREATE INDEX ON auth.workzone USING GIST(path)");
        DB::statement("CREATE INDEX ON auth.workzone USING btree(path)");

        DB::statement("INSERT INTO auth.workzone(label, path) VALUES('TELKOM Indonesia', '1')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TABLE auth.workzone");
    }
}
