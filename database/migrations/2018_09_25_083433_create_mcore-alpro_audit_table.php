<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreAlproAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.alpro_audit(
              id BIGSERIAL PRIMARY KEY,
              alpro_id INTEGER REFERENCES mcore.alpro(id),
              user_id INTEGER REFERENCES auth.user(id),
              operation TEXT NOT NULL CHECK (operation <> ''),
              timestamp TIMESTAMP WITH TIME ZONE, 
              data JSON NOT NULL
            )
        ");

        DB::statement("CREATE INDEX ON mcore.alpro_audit(alpro_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.alpro_audit');
    }
}
