<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.link(
              src_type TEXT,
              src_id BIGINT,
              src_val TEXT DEFAULT '',
              
              dst_type TEXT,
              dst_id BIGINT,
              dst_val TEXT DEFAULT '',
              
              med_type TEXT,
              med_id BIGINT,
              med_val TEXT DEFAULT '',
              
              attr JSONB,
              
              PRIMARY KEY (src_type, src_id, src_val, dst_type, dst_id, dst_val)
            )
        ");

        DB::statement('CREATE INDEX ON mcore.link(src_type, src_id)');
        DB::statement('CREATE INDEX ON mcore.link(dst_type, dst_id)');
        DB::statement('CREATE INDEX ON mcore.link(med_type, med_id)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.link');
    }
}
