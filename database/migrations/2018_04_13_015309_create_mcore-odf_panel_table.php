<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreOdfPanelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.odf_panel(
              id BIGSERIAL PRIMARY KEY,
              odf_id BIGINT REFERENCES mcore.odf(id),
              label TEXT,
              slotcount SMALLINT,
              slotportcount SMALLINT,
              slotvertical BOOLEAN DEFAULT TRUE
            )
        ");

        DB::statement("CREATE INDEX ON mcore.odf_panel(odf_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.odf_panel');
    }
}
