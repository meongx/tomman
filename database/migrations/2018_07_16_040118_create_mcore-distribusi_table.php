<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreDistribusiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.distribusi(
              id BIGSERIAL PRIMARY KEY,
              workzone_id SMALLINT REFERENCES auth.workzone(id),
              label TEXT NOT NULL CHECK (label <> ''),
              capacity SMALLINT,
              branch_path LTREE,
              route_path JSON
            )
        ");

        DB::statement("CREATE INDEX ON mcore.distribusi USING GIST(branch_path)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.distribusi');
    }
}
