<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAuthPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE auth.permission(
              id          SERIAL PRIMARY KEY,
              title       text NOT NULL CHECK (title <> ''),
              permission  text NOT NULL CHECK (permission <> '')
            )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TABLE auth.permission");
    }
}
