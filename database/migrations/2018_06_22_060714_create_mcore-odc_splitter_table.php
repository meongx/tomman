<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreOdcSplitterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.odc_splitter(
              id BIGSERIAL PRIMARY KEY,
              odc_id BIGINT REFERENCES mcore.odc(id),
              label TEXT NOT NULL CHECK (label <> '')
            )
        ");

        DB::statement("CREATE INDEX ON mcore.odc_splitter(odc_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.odc_splitter');
    }
}
