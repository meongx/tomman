<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMcoreStoAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.sto_audit(
              id BIGSERIAL PRIMARY KEY,
              sto_id INTEGER REFERENCES mcore.sto(id),
              user_id INTEGER REFERENCES auth.user(id),
              operation TEXT NOT NULL CHECK (operation <> ''),
              timestamp TIMESTAMP WITH TIME ZONE, 
              data JSON NOT NULL
            )
        ");

        DB::statement("CREATE INDEX ON mcore.sto_audit(sto_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.sto_audit');
    }
}
