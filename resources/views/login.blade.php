<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TOMMANv2</title>

    <link rel="stylesheet" href="{{ _url('/css/vendor.css') }}">
    <link rel="stylesheet" href="{{ _url('/css/app.css') }}">

    <style>
      .page-signin-header {
        box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
      }

      .page-signin-container {
        width: auto;
        margin: 30px 10px;
      }

      .page-signin-container form {
        border: 0;
        box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
      }

      @media (min-width: 544px) {
        .page-signin-container {
          width: 350px;
          margin: 60px auto;
        }
      }

    </style>
  </head>

  <body>
    <div class="page-signin-header p-a-2 text-sm-center bg-white">
      <span style="font-size:24px;font-weight:bold;">TOMMANv2</span>
    </div>

    @include('partial.alerts')

    <div class="page-signin-container" id="page-signin-form">
      <form class="panel p-a-4" method="post">
        {{ csrf_field() }}

        <fieldset class="form-group form-group-lg">
          <input name="nik" type="text" class="form-control" placeholder="NIK" value="{{ old('nik') }}" autofocus required>
        </fieldset>

        <fieldset class="form-group form-group-lg">
          <input name="pass" type="password" class="form-control" placeholder="Password" required>
        </fieldset>

        <div class="clearfix">
          <label class="custom-control custom-checkbox pull-xs-left">
            <input name="remember" type="checkbox" class="custom-control-input">
            <span class="custom-control-indicator"></span>
            <span>Remember me</span>
          </label>
        </div>

        <button class="btn btn-block btn-lg btn-primary m-t-3">Login</button>
      </form>
    </div>

  </body>
</html>
