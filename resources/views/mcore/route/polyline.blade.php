<script>
  window.route.polyline = {
    construct: function(map) {
      const mapLine = new google.maps.Polyline({
        map,
        clickable: false,
        strokeColor: 'red',
        strokeOpacity: .7,
        strokeWeight: 10
      });

      const addPoint = latLng => mapLine.getPath().push(latLng);
      const removePointByIndex = index => mapLine.getPath().removeAt(index);
      const movePoint = (oldIndex, newIndex) => {
        const path = mapLine.getPath();
        const item = path.removeAt(oldIndex);
        path.insertAt(newIndex, item);
      };

      return {
        addPoint,
        removePointByIndex,
        movePoint
      };
    }
  }
</script>
