<script>
  if (!window.Alpro) window.Alpro = {};
  window.Alpro.init = (() => {
    const c = window.McoreMap.const;

    const hiddenClass = 'hidden';

    const $mapView = $('#alpro_map');
    const mapView = $mapView[0];

    const $btnStartInput = $('#btn-start-input');
    const $btnCenter = $('#btn-center');

    const $frmAlpro = $('#frm-alpro');
    const $inputCoordinate = $('#input-coordinate');
    const $btnCancel = $('#btn-cancel');

    /*
     * Page Layout
     */
    const layoutPage = () => {
      const sidebar = $('.px-nav-left');
      const sidebarLeft = sidebar.offset().left;
      const footerHeight = $('.px-footer-bottom').outerHeight();
      const navbarHeight = $('.navbar').outerHeight();

      if (sidebarLeft === 0) {
        const margin = 10;
        const left = sidebar.outerWidth() + margin;
        const right = margin;
        const bottom = footerHeight + margin;
        const top = navbarHeight + (margin * 2);

        $('#btn-center').css('bottom', 175);
        $('#container-alert').css({ left, right, top });
        $frmAlpro.css({ left, right, bottom });
        $btnStartInput.css('top', margin);
      }

      const substracter = PixelAdmin.isMobile ? footerHeight : navbarHeight;
      $mapView.css('height', window.innerHeight - substracter);
    };

    /*
     * Position Tracker
     */
    let tracker;
    let posWatch;

    const showTracker = latlng => {
      if (!tracker) {
        tracker = new google.maps.Marker({
          position: latlng,
          map: window.map,
          animation: google.maps.Animation.BOUNCE
        });
      } else {
        if (!tracker.getVisible()) {
          tracker.setVisible(true);
          tracker.setAnimation(google.maps.Animation.BOUNCE);
        }
        tracker.setPosition(latlng);
      }
    };

    const panToTracker = () => {
      if (!tracker) return;

      const latlng = tracker.getPosition();
      window.map.panTo(latlng);
    };

    const onPosUpdated = event => {
      const pos = event.coords;
      const latlng = {
        lat: pos.latitude,
        lng: pos.longitude
      };
      showTracker(latlng);

      if (window.map.getZoom() === c.defaultZoom) {
        window.map.setZoom(19);
        window.map.panTo(latlng);
      }
    };

    const onPosError = error => {
      console.log('onPosError', error);
    };

    const startTracking = () => {
      posWatch = navigator.geolocation.watchPosition(onPosUpdated, onPosError, { enableHighAccuracy: true });
    };

    const stopTracking = () => {
      navigator.geolocation.clearWatch(posWatch);
      tracker.setVisible(false);
    };

    /*
     * Draggable Marker
     */
    let marker;

    const onDragEnd = event => {
      const latlng = event.latLng;
      $inputCoordinate.val(latlng.lat() + ', ' + latlng.lng());
    };

    const showMarker = latlng => {
      if (!marker) {
        marker = new google.maps.Marker({
          position: latlng,
          map: window.map,
          draggable: true,
          icon: '/img/mcore/marker-draggable.png'
        });
        marker.addListener('dragend', onDragEnd)
      } else {
        marker.setPosition(latlng);
        marker.setVisible(true);
      }
    };

    const startInputMode = () => {
      if (!tracker) return;

      const latlng = tracker.getPosition();
      window.map.panTo(latlng);
      showMarker(latlng);

      stopTracking();
      $btnStartInput.addClass(hiddenClass);
      $frmAlpro.removeClass(hiddenClass);

      $inputCoordinate.val(latlng.lat() + ', ' + latlng.lng());
    };

    const endInputMode = () => {
      $frmAlpro.addClass(hiddenClass);
      $btnStartInput.removeClass(hiddenClass);

      marker.setVisible(false);
      startTracking();
    };

    /*
     * Map Initializer
     */
    return () => {
      layoutPage();

      $btnStartInput.on('click', startInputMode);
      $btnCenter.on('click', panToTracker);
      $btnCancel.on('click', endInputMode);

      window.map = new google.maps.Map(mapView, {
        center: c.center,
        zoom: c.defaultZoom,
        minZoom: c.defaultZoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        clickableIcons: false,
        fullscreenControl: false,
        streetViewControl: false,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        }
      });

      window.Alpro.grid.init();
      startTracking();
    };
  })();
</script>
