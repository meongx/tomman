@extends('app')

<?php $title = $odfData->label ?? 'Input ODF' ?>
@section('title', $title)

@section('style')
  <style>
    .odf_panel {
      margin-bottom: 20px;
    }
    .odf_panel .title {
      font-size: large;
      text-align: center;
    }
    .odf_panel .flex-h {
      justify-content: center;
    }
    .odf_panel .flex-v {
      margin-right: 0;
    }

    .port.green { background-color: #00ff00 }
    .port.yellow { background-color: yellow }
    .port.red { background-color: red }

    @media screen and (max-width: 768px) {
      .odf_panel .flex-h {
        justify-content: flex-start;
      }
    }
  </style>
@endsection

@section('body')
  <ol class="breadcrumb page-breadcrumb">
    <li>
      <a href="/mcore/sto/workzone/{{ $stoData->workzone_id }}">
        <span class="label label-primary">WZ</span>
        <span>{{ $stoData->workzone_label }}</span>
      </a>
    </li>
    <li>
      <a href="/mcore/sto/{{ $stoData->id }}">
        <span class="label label-primary">STO</span>
        <span>{{ $stoData->label }}</span>
      </a>
    </li>
    <li>
      <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}">
        <span class="label label-primary">room</span>
        <span>{{ $roomData->label }}</span>
      </a>
    </li>
    <li class="active">
      @if (isset($odfData))
        <span class="label label-primary">ODF</span>
        <span>{{ $odfData->label }}</span>
      @else
        ODF
      @endif
    </li>
  </ol>

  <div class="page-header">
    <h1>
      <i class="fas fa-edit"></i>
      <span>{{ $title }}</span>
    </h1>
  </div>

  <form id="form" class="panel" method="post">
    {{ csrf_field() }}
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-6">
          @include('partial.form.text', [
            'label' => 'Nama/Label ODF',
            'object' => $odfData,
            'field' => 'label',
            'canEdit' => $canEdit,
            'attributes' => [
              'required' => true,
              'data-msg-required' => 'Silahkan isi data ini'
            ]
          ])
        </div>

        <div class="col-sm-6">
          @include('partial.form.text', [
            'label' => 'Group',
            'object' => $odfData,
            'field' => 'group',
            'canEdit' => $canEdit,
            'help' => 'Misal: Baris-1, O-SIDE, Kolom-Barat, E-TRANS, dsb'
          ])
        </div>
      </div>
    </div>

    @if ($canEdit)
      <div class="panel-footer text-right">
        <button class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    @endif
  </form>

  @if (isset($odfData))
    <h3>
      Optical Terminal Box

      @if ($canEdit)
        <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}/odf/{{ $odfData->id }}/panel/new"
           class="btn btn-info">
          <i class="fas fa-plus"></i>
          <span>Panel</span>
        </a>
      @endif
    </h3>

    @foreach($panelList as $otb)
      <div class="odf_panel">
        <div class="overflow-h flex-{{ $otb->slotvertical?'h':'v' }}">
          @for ($i = 1; $i <= $otb->slotcount; $i++)
            <div class="flex-{{ $otb->slotvertical?'v':'h' }}">
              @for ($p = 1; $p <= $otb->slotportcount; $p++)
                <?php $port = (($i - 1) * $otb->slotportcount) + $p ?>
                <div class="port">{{ $port }}</div>
              @endfor
            </div>
          @endfor
        </div>
        <div class="title">
          <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}/odf/{{ $odfData->id }}/panel/{{ $otb->id }}">
            {{ $otb->label }}
          </a>
        </div>
      </div>
    @endforeach
  @endif

@endsection

@section('script')
  @include('partial.form.validate', ['id' => 'form'])
@endsection
