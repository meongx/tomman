@if (count($splitterList))
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">Splitter</h4>
    </div>
    <div class="panel-body">
      <ul id="splitter-list" class="list-splitter">
        <?php $colorIndex = 1; $maxColors = 9; ?>
        @foreach($splitterList as $splitter)
          <?php $splitter->colorIndex = $colorIndex++ ?>
          <?php if ($colorIndex > $maxColors) $colorIndex = 1 ?>

          <li class="bg-fiber-{{ $splitter->colorIndex }}"
              data-splitter-id="{{ $splitter->id }}" data-splitter-label="{{ $splitter->label }}">
            {{ $splitter->label }}

            <div class="hidden">
              <div class="title">
                <span>Splitter</span>
                <span class="label bg-fiber-{{ $splitter->colorIndex }}">{{ $splitter->label }}</span>
              </div>
              <div class="body">
                @for ($i = 0; $i <= 4; $i++)
                  <?php $link = $splitter->ports[$i] ?? false ?>
                  @if($link)
                    <?php $token = ($i <= 0) ? $link->src_val : $link->dst_val ?>
                    <?php $token = explode(':', $token) ?>
                  @endisset

                  <?php $type = ($i <= 0) ? 'in' : 'out' ?>

                  <div class="splitter-port {{ ($link) ? 'clickable' : '' }}"
                       data-odc-panel="{{ $token[0] ?? '' }}" data-odc-port="{{ $token[1] ?? '' }}">
                    <span class="label label-success">{{ $type }}</span>
                    <span class="label label-info label-outline">{{ ($i <= 0) ? 'IN' : $i }}</span>
                    <i class="fas fa-long-arrow-alt-{{ ($i <= 0) ? 'left' : 'right' }}"></i>

                    @if ($link)
                      <span>
                      Panel
                      <span class="label label-info label-outline">{{ $token[0] }}</span>
                      Port
                      <span class="label label-info label-outline">{{ $token[1] }}</span>
                    </span>
                    @else
                      <span>UNPLUGGED</span>
                    @endisset
                  </div>

                  @if ($i == 0)
                    <hr>
                  @endif
                @endfor
              </div>
            </div>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
@endif

<div id="panelports" class="overflow-h m-t-4">
  @foreach($panelPorts as $panelNum => $panel)
    <div class="odc-panel flex-h">
      <div class="caption">
        PANEL {{ $panelNum }}
      </div>

      @foreach($panel as $port => $link)
        <div class="port-column">
          <div id="panel-{{ $panelNum }}-port-{{ $port }}-rear"
               class="rearport"
               data-port="{{ $port }}" data-panel="{{ $panelNum }}">
            <i class="fas fa-sort-up"></i>
          </div>

          <?php $class = '' ?>
          @if ($link->front)
            <?php $class = 'up bg-fiber-'.$link->front->dst_obj->colorIndex ?>
            @if ($link->front->dst_type === \App\Service\Mcore\Link\OdcSplitter::TYPE_SPLIITER)
              <?php $class .= ' in' ?>
            @endif
          @endif
          <div id="panel-{{ $panelNum }}-port-{{ $port }}-front"
               class="port {{ $class }}"
               data-port="{{ $port }}" data-panel="{{ $panelNum }}">
            {{ $port }}

            @if ($link->front)
              <div class="hidden">
                <div class="splitter-view">
                  <span class="label bg-fiber-{{ $link->front->dst_obj->colorIndex }}">
                    {{ $link->front->dst_obj->label }}
                  </span>
                </div>
                <div class="splitter-port-view">
                  @if ($link->front->dst_val <= 0)
                    <?php $splitterPortType = 'in'; $splitterPortLabel = 'IN' ?>
                  @else
                    <?php $splitterPortType = 'out'; $splitterPortLabel = $link->front->src_val ?>
                  @endif
                  <span class="label label-success">{{ $splitterPortType }}</span>
                  <span>port</span>
                  <span class="label label-outline label-info">{{ $splitterPortLabel }}</span>
                </div>
              </div>
            @endif
          </div>
        </div>
      @endforeach
    </div>
  @endforeach
</div>

<div id="splitter-modal" class="modal" tabindex="-1">
  <div class="modal-dialog">
    <form id="form-splitter-edit" class="modal-content" action="/mcore/odc/{{ $odcData->id }}/splitter" method="post">
      {{ csrf_field() }}
      <input id="input-splitter_edit_id" name="splitter_id" type="hidden">

      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
        <div class="modal-title"></div>
      </div>

      <div class="modal-body">
        <fieldset class="form-group">
          <label for="input-splitter_edit_label">Label/Nama/Kode Splitter</label>
          <input id="input-splitter_edit_label" name="splitter_label" class="form-control">
        </fieldset>

        <label>Ports</label>
        <div class="splitter-port-view" style="margin-top: -10px"></div>
      </div>

      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="reset" data-dismiss="modal">
          <i class="fas fa-ban"></i>
          <span>Batal</span>
        </button>

        <button class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    </form>
  </div>
</div>

<div id="frontlink-modal" class="modal" tabindex="-1">
  <div class="modal-dialog">
    <form id="form-splitter" class="modal-content" action="/mcore/odc/{{ $odcData->id }}/splitter" method="post">
      {{ csrf_field() }}
      <input id="input-odc-panel" name="odc_panel" type="hidden">
      <input id="input-odc-port" name="odc_port" type="hidden">

      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>

      <div class="modal-body">
        <fieldset class="form-group form-message-light">
          <label>
            <span id="label-splitter-new" class="hidden">Label/Nama/Kode</span>
            <span>Splitter</span>
          </label>
          <div class="row">
            <div class="col-md-9">
              <input id="input-splitter-label" name="splitter_label" class="form-control hidden"
                     data-msg-required="Silahkan isi data ini">
              <div id="container-splitter">
                <select id="input-splitter" name="splitter_id" class="form-control"
                        data-msg-required="Silahkan isi data ini">
                </select>
              </div>
              <p id="readonly-splitter-label" class="form-control-static hidden"></p>
            </div>
            <div class="col-md-3">
              <label id="check-splitter-new" class="custom-control custom-checkbox m-xs-t-10 m-md-t-5">
                <input id="input-splitter_new" name="splitter_new" class="custom-control-input" type="checkbox">
                <span class="custom-control-indicator"></span>
                <span>Splitter Baru</span>
              </label>
              <button id="btn-frontlink-unplug" name="unplug" value="1" class="btn btn-danger pull-right hidden">
                <i class="fas fa-unlink"></i>
                <span>Cabut</span>
              </button>
            </div>
          </div>
          <small id="text-splitter-descriptor" class="text-muted">
            Pilih Splitter yang sudah ada atau input Splitter Baru
          </small>
        </fieldset>

        <fieldset class="form-group form-message-light">
          <label>Port Splitter</label>
          <div id="container-splitter-port">
            <select name="splitter_port" id="input-splitter-port" class="form-control"
                    required data-msg-required="Silahkan isi data ini">
            </select>
          </div>
          <i id="port-loading" class="fas fa-spinner fa-pulse hidden"></i>
          <p id="readonly-splitter-port" class="form-control-static hidden"></p>
        </fieldset>
      </div>

      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="reset" data-dismiss="modal">
          <i class="fas fa-ban"></i>
          <span>Batal</span>
        </button>

        <button id="btn-frontlink-save" class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    </form>
  </div>
</div>
