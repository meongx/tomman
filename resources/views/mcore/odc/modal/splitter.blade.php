<div id="splitter-modal" class="modal" tabindex="-1">
  <div class="modal-dialog">
    <form id="form-splitter-edit" class="modal-content" action="/mcore/odc/{{ $odcData->id }}/splitter" method="post">
      {{ csrf_field() }}
      <input id="input-splitter_edit_id" name="splitter_id" type="hidden">

      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
        <div class="modal-title"></div>
      </div>

      <div class="modal-body">
        <fieldset class="form-group">
          <label for="input-splitter_edit_label">Label/Nama/Kode Splitter</label>
          <input id="input-splitter_edit_label" name="splitter_label" class="form-control" autocomplete="off">
        </fieldset>

        <label>Ports</label>
        <div class="splitter-port-view" style="margin-top: -10px"></div>
      </div>

      <div class="modal-footer">
        <button class="btn btn-default pull-left" type="reset" data-dismiss="modal">
          <i class="fas fa-ban"></i>
          <span>Batal</span>
        </button>

        <button class="btn btn-primary">
          <i class="fas fa-check"></i>
          <span>Simpan</span>
        </button>
      </div>
    </form>
  </div>
</div>
