<script>
  $(() => {
    const $panelPorts = window.$panelPorts;
    const $rearLinkModal = $('#rearlink-modal');
    const $rearLinkModalTitle = $rearLinkModal.find('.modal-title');
    const $existsLinkView = $rearLinkModal.find('.exists-link-view');
    const $newLinkView = $rearLinkModal.find('.new-link-view');
    const $btnLinkToOdf = $('#btn-link-odf');
    const $btnLinkToOdp = $('#btn-link-odp');

    const hiddenClass = window.hiddenClass;

    let $currentPortHidden;
    let $currentPortLinkView;

    $panelPorts.on('click', '.rearport', event => {
      const $target = $(event.currentTarget);

      const panel = $target.data('panel');
      const port = $target.data('port');

      $rearLinkModalTitle.text(`Panel ${panel} Port ${port}`);

      if ($target.hasClass('up')) {
        $currentPortHidden = $target.children('.hidden');
        $currentPortLinkView = $currentPortHidden.children('.link-view').detach();

        $existsLinkView.removeClass(hiddenClass).append($currentPortLinkView);
        $newLinkView.addClass(hiddenClass);
      } else {
        $currentPortHidden = $currentPortLinkView = null;

        $existsLinkView.addClass(hiddenClass);
        $newLinkView.removeClass(hiddenClass);

        $btnLinkToOdf.attr('href', `/mcore/odc/{{ $odcData->id }}/panel/${panel}/port/${port}/odf`);
        $btnLinkToOdp.attr('href', `/mcore/odc/{{ $odcData->id }}/panel/${panel}/port/${port}/odp`);
      }

      $rearLinkModal.modal('show');
    });

    $rearLinkModal.on('hidden.bs.modal', event => {
      if (!$currentPortHidden) return;

      $currentPortLinkView.detach().appendTo($currentPortHidden);
    });
  });
</script>
