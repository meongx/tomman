<script>
  window.hiddenClass = 'hidden';

  window.$splitterList = $('#splitter-list');
  window.$splitterModal = $('#splitter-modal');
  window.$splitterModalTitle = window.$splitterModal.find('.modal-title');
  window.$splitterModalBody = window.$splitterModal.find('.splitter-port-view');

  window.$inputSplitterEditId = $('#input-splitter_edit_id');
  window.$inputSplitterEditLabel = $('#input-splitter_edit_label');

  window.$panelPorts = $('#panelports');
  window.$linkModal = $('#frontlink-modal');
  window.$linkModalTitle = window.$linkModal.find('.modal-title');

  window.$formSplitter = $('#form-splitter');

  window.$inputOdcPanel = $('#input-odc-panel');
  window.$inputOdcPort = $('#input-odc-port');

  window.$labelSplitterNew = $('#label-splitter-new');
  window.$inputSplitterLabel = $('#input-splitter-label');
  window.$readonlySplitterLabel = $('#readonly-splitter-label');

  window.$containerSplitter = $('#container-splitter');
  window.$inputSplitter = $('#input-splitter');

  window.$inputSplitterNew = $('#input-splitter_new');
  window.$inputSplitterPort = $('#input-splitter-port');
  window.$readonlySplitterPort = $('#readonly-splitter-port');
  window.$containerSplitterPort = $('#container-splitter-port');

  window.$checkSplitterNew = $('#check-splitter-new');
  window.$textSplitterDescriptor = $('#text-splitter-descriptor');

  window.$btnFrontLinkUnplug = $('#btn-frontlink-unplug');
  window.$btnFrontLinkSave = $('#btn-frontlink-save');

  window.escapeMarkup = m => m;
</script>
