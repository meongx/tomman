@extends('app')

@section('body')
  <ol class="breadcrumb page-breadcrumb">
    <li>
      <a href="/mcore/sto/{{ $stoData->id }}">
        <span class="label label-info">STO</span>
        <span>{{ $stoData->label }}</span>
      </a>
    </li>
    <li>
      <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}">
        <span class="label label-info">room</span>
        <span>{{ $roomData->label }}</span>
      </a>
    </li>
    <li class="active">New OLT</li>
  </ol>


@endsection
