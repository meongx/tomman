@extends('app')

@section('title', 'Sambungan Feeder')

@section('body')
  @if(Request::is('*/odc/*'))
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/odp/workzone/{{ $linkData->odc_workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $linkData->odc_workzone_label }}</span>
        </a>
      </li>
      <li>
        <a href="/mcore/odc/{{ $linkData->odc_id }}">
          <span class="label label-primary">ODC</span>
          <span>{{ $linkData->odc_label }}</span>
        </a>
      </li>
      <li class="active">
        <span>PANEL</span>
        <span class="label label-outline label-info">{{ $linkData->odc_panel }}</span>
        <span>PORT</span>
        <span class="label label-outline label-info">{{ $linkData->odc_port }}</span>
      </li>
    </ol>
  @else
    <ol class="breadcrumb page-breadcrumb">
      <li>
        <a href="/mcore/sto/workzone/{{ $linkData->sto_workzone_id }}">
          <span class="label label-primary">WZ</span>
          <span>{{ $linkData->sto_workzone_label }}</span>
        </a>
      </li>
      <li>
        <?php $url = '/mcore/sto/'.$linkData->sto_id ?>
        <a href="{{ $url }}">
          <span class="label label-primary">STO</span>
          <span>{{ $linkData->sto_label }}</span>
        </a>
      </li>
      <li>
        <?php $url .= '/room/'.$linkData->sto_room_id ?>
        <a href="{{ $url }}">
          <span class="label label-primary">room</span>
          <span>{{ $linkData->sto_room_label }}</span>
        </a>
      </li>
      <li>
        <?php $url .= '/odf/'.$linkData->odf_id ?>
        <a href="{{ $url }}">
          <span class="label label-primary">ODF</span>
          <span>{{ $linkData->odf_label }}</span>
        </a>
      </li>
      <li>
        <?php $url .= '/panel/'.$linkData->odf_panel_id ?>
        <a href="{{ $url }}">
          <span class="label label-primary">OTB</span>
          <span>{{ $linkData->odf_panel_label }}</span>
        </a>
      </li>
      <li class="active">
        <span>Port</span>
        <span class="label label-outline label-info">{{ $linkData->odf_panel_port }}</span>
      </li>
    </ol>
  @endisset

  <div class="page-header">
    <h1>
      <i class="fas fa-share-alt"></i>
      <span>Sambungan Feeder</span>
    </h1>
  </div>

  <form id="form" method="post" class="panel">
    {{ csrf_field() }}

    <div class="panel-body">
      <div class="row">
        <div id="sto-form-group" class="col-md-4">
          @isset($linkData->odf_panel_id)
            <input name="src_id" value="{{ $linkData->src_id }}" type="hidden">
            <input name="src_val" value="{{ $linkData->src_val }}" type="hidden">

            <div class="form-group">
              <label>STO</label>
              <p class="form-control-static">
                <?php $url = '/mcore/sto/'.$linkData->sto_id ?>
                <a href="{{ $url }}">
                  {{ $linkData->sto_label }}
                </a>
              </p>
            </div>

            <div class="form-group">
              <label>Ruangan</label>
              <p class="form-control-static">
                <?php $url .= '/room/'.$linkData->sto_room_id ?>
                <a href="{{ $url }}">
                  {{ $linkData->sto_room_label }}
                </a>
              </p>
            </div>

            <div class="form-group">
              <label>ODF</label>
              <p class="form-control-static">
                <?php $url .= '/odf/'.$linkData->odf_id ?>
                <a href="{{ $url }}">
                  {{ $linkData->odf_label }}
                </a>
              </p>
            </div>

            <div class="form-group">
              <label>Panel / OTB</label>
              <p class="form-control-static">
                <?php $url .= '/panel/'.$linkData->odf_panel_id ?>
                <a href="{{ $url }}">
                  {{ $linkData->odf_panel_label }}
                </a>
              </p>
            </div>

            <div class="form-group">
              <label>Port</label>
              <p class="form-control-static">
                <span>Port</span>
                <span class="label label-info label-outline">{{ $linkData->odf_panel_port }}</span>
              </p>
            </div>
          @else
            @include('partial.form.select.keyval', [
              'label' => 'STO',
              'object' => $linkData,
              'field' => 'sto_id',
              'options' => [],
              'canEdit' => true,
              'attributes' => [
                'required' => true,
                'data-msg-required' => 'Silahkan pilih STO'
              ]
            ])

            @include('partial.form.select.keyval', [
              'label' => 'Ruangan',
              'object' => $linkData,
              'field' => 'room_id',
              'options' => [],
              'canEdit' => true,
              'attributes' => [
                'required' => true,
                'data-msg-required' => 'Silahkan pilih Ruangan STO'
              ]
            ])

            @include('partial.form.select.keyval', [
              'label' => 'ODF',
              'object' => $linkData,
              'field' => 'odf_id',
              'options' => [],
              'canEdit' => true,
              'attributes' => [
                'required' => true,
                'data-msg-required' => 'Silahkan pilih ODF'
              ]
            ])

            @include('partial.form.select.keyval', [
              'label' => 'Panel / OTB',
              'object' => $linkData,
              'field' => 'src_id',
              'options' => [],
              'canEdit' => true,
              'attributes' => [
                'required' => true,
                'data-msg-required' => 'Silahkan pilih OTB'
              ]
            ])

            @include('partial.form.select.keyval', [
              'label' => 'Port',
              'object' => $linkData,
              'field' => 'src_val',
              'options' => [],
              'canEdit' => true,
              'attributes' => [
                'required' => true,
                'data-msg-required' => 'Silahkan pilih Port'
              ]
            ])
          @endisset
        </div>

        <div id="feeder-form-group" class="col-md-4">
          @isset($linkData->feeder_id)
            <input name="med_id" value="{{ $linkData->med_id }}" type="hidden">
            <input name="med_val" value="{{ $linkData->med_val }}" type="hidden">

            <div class="form-group">
              <label>Feeder</label>
              <p class="form-control-static">
                <a href="/mcore/feeder/{{ $linkData->feeder_id }}">
                  {{ $linkData->feeder_label }}
                </a>
              </p>
            </div>

            <div class="form-group">
              <label>Core</label>
              @include('mcore.link.partial.tubecore', \App\Service\Mcore\Helper::getCoreData($linkData->med_val))
            </div>
          @else
            <div>
              <label>Kabel Feeder</label>
              <div class="row">
                <div class="col-xs-10">
                  @include('partial.form.select.keyval', [
                    'object' => $linkData,
                    'field' => 'med_id',
                    'options' => [],
                    'canEdit' => true,
                    'attributes' => [
                      'required' => true,
                      'data-msg-required' => 'Silahkan pilih Kabel Feeder'
                    ]
                  ])
                </div>
                <div class="col-xs-2">
                  <a href="/mcore/feeder/new?ret={{ url()->full() }}" class="btn btn-info pull-right">
                    <i class="fas fa-plus"></i>
                  </a>
                </div>
              </div>
            </div>

            <div id="core-form-group">
              @include('partial.form.select.keyval', [
                'label' => 'Core',
                'object' => $linkData,
                'field' => 'med_val',
                'options' => [],
                'canEdit' => true,
                'attributes' => [
                  'required' => true,
                  'data-msg-required' => 'Silahkan pilih Core'
                ]
              ])
            </div>
          @endisset
        </div>

        <div id="odc-form-group" class="col-md-4">
          @isset($linkData->odc_id)
            <input name="dst_id" value="{{ $linkData->odc_id }}" type="hidden">
            <input name="dst_val" value="{{ $linkData->dst_val }}" type="hidden">

            <div class="m-md-l-10">
              <div class="form-group">
                <label>ODC</label>
                <div class="form-control-static">
                  <input name="src_id" value="{{ $linkData->odc_id }}" type="hidden">
                  <a href="/mcore/odc/{{ $linkData->odc_id }}">{{ $linkData->odc_label }}</a>
                </div>
              </div>

              <div class="form-group">
                <label>Panel &amp; Port</label>
                <div class="form-control-static">
                  <input name="dst_val" value="{{ $linkData->dst_val }}" type="hidden">
                  <span>Panel</span>
                  <span class="label label-outline label-info">{{ $linkData->odc_panel }}</span>
                  <span>Port</span>
                  <div class="label label-outline label-info">{{ $linkData->odc_port }}</div>
                </div>
              </div>
            </div>
          @else
            @include('partial.form.select.keyval', [
              'label' => 'ODC',
              'object' => $linkData,
              'field' => 'dst_id',
              'options' => [],
              'canEdit' => true,
              'attributes' => [
                'required' => true,
                'data-msg-required' => 'Silahkan pilih ODC'
              ]
            ])

            @include('partial.form.select.keyval', [
              'label' => 'Panel & Port',
              'object' => $linkData,
              'field' => 'dst_val',
              'options' => [],
              'canEdit' => true,
              'attributes' => [
                'required' => true,
                'data-msg-required' => 'Silahkan pilih Port ODC'
              ]
            ])
          @endisset
        </div>
      </div>
    </div>

    @if ($canEdit)
      @isset($linkData->med_id)
        <div class="panel-footer">
          <button name="action" value="unplug" type="submit" class="btn btn-warning">
            <i class="fas fa-unlink"></i>
            <span>Cabut Sambungan</span>
          </button>
        </div>
      @else
        <div class="panel-footer text-right">
          <button name="action" value="plug" type="submit" class="btn btn-primary">
            <i class="fas fa-check"></i>
            <span>Simpan</span>
          </button>
        </div>
      @endisset
    @endif
  </form>
@endsection

@section('script')
  <script>
    $(() => {
      const $stoFormGroup = $('#sto-form-group');
      const $stoInput = $('#input-sto_id');
      const $roomInput = $('#input-room_id');
      const $odfInput = $('#input-odf_id');
      const $otbInput = $('#input-src_id');
      const $otbPortInput = $('#input-src_val');

      const $feederFormGroup = $('#feeder-form-group');
      const $feederInput = $('#input-med_id');
      const $coreInput = $('#input-med_val');

      const $odcFormGroup = $('#odc-form-group');
      const $odcInput = $('#input-dst_id');
      const $odcPortInput = $('#input-dst_val');

      const placeholder = '&nbsp;';
      const escapeMarkup = m => m;
      const pagedDataHandler = params => {
        return {
          q: params.term,
          page: params.page || 1
        }
      };

      const attachSelect2Listener = ($container, $inputSource, $inputTarget, urlGenerator) => {
        $inputSource.on('select2:select', event => {
          if ($inputTarget.hasClass('select2-hidden-accessible')) {
            $inputTarget.select2('destroy').empty();
          }
          $container.addClass('form-loading form-loading-inverted');

          const url = urlGenerator(event);
          $.get(url, data => {
            $inputTarget.select2({
              data,
              placeholder,
              escapeMarkup
            });
            $inputTarget.val(null).trigger('change');

            $container.removeClass('form-loading form-loading-inverted');
          });
        });
      };

      /*
      STO, ROOM, ODF, OTB, Port
       */
      $stoInput.select2({
        ajax: {
          url: '/mcore/sto/workzone/{{ $linkData->odc_workzone_id ?? $linkData->sto_workzone_id }}.select2',
          data: pagedDataHandler
        },
        placeholder,
        escapeMarkup
      });

      attachSelect2Listener(
        $stoFormGroup,
        $stoInput,
        $roomInput,
        event => '/mcore/sto/'+event.params.data.id+'/room.select2'
      );

      attachSelect2Listener(
        $stoFormGroup,
        $roomInput,
        $odfInput,
        event => '/mcore/sto/room/'+event.params.data.id+'/odf.select2'
      );

      attachSelect2Listener(
        $stoFormGroup,
        $odfInput,
        $otbInput,
        event => '/mcore/sto/room/odf/'+event.params.data.id+'/otb.select2'
      );

      attachSelect2Listener(
        $stoFormGroup,
        $otbInput,
        $otbPortInput,
        event => '/mcore/sto/room/odf/otb/'+event.params.data.id+'/port.select2'
      );

      /*
      Feeder, Core
       */
      $feederInput.select2({
        ajax: {
          url: '/mcore/feeder/workzone/{{ $linkData->odc_workzone_id ?? $linkData->sto_workzone_id }}.select2',
          data: pagedDataHandler
        },
        placeholder,
        escapeMarkup
      });

      attachSelect2Listener(
        $feederFormGroup,
        $feederInput,
        $coreInput,
        event => '/mcore/feeder/'+event.params.data.id+'/core.select2'
      );

      /*
      ODC, Panel, Port
       */
      $odcInput.select2({
        ajax: {
          url: '/mcore/odc/workzone/{{ $linkData->odc_workzone_id ?? $linkData->sto_workzone_id }}.select2',
          data: pagedDataHandler
        },
        placeholder,
        escapeMarkup
      });

      attachSelect2Listener(
        $odcFormGroup,
        $odcInput,
        $odcPortInput,
        event => '/mcore/odc/'+event.params.data.id+'/rearport.select2'
      );
    });
  </script>

  @include('partial.form.validate', ['id' => 'form'])
@endsection
