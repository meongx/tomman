<?php $colors = \App\Service\Mcore\Helper::COLORS ?>
<div class="m-b-1">
  <div class="block-md-inline">
    <span class="port">Tube</span>
    <span class="port outline-fiber-{{ $tubeNum }}">{{ $tubeNum }}</span>
    <span class="port bg-fiber-{{ $tubeNum }}">{{ $colors[$tubeNum] }}</span>
  </div>
  <div class="block-md-inline m-md-l-10 m-xs-t-10">
    <span class="port">Core</span>
    <span class="port outline-fiber-{{ $coreNum }}">{{ $coreNum }}</span>
    <span class="port bg-fiber-{{ $coreNum }}">{{ $colors[$coreNum] }}</span>
    <span class="m-l-1">#{{ $coreId }}</span>
  </div>
</div>
<div class="tubecore">
  <div class="bg-fiber-{{ $tubeNum }} tube"></div>
  <div class="bg-fiber-{{ $coreNum }} core"></div>
</div>

