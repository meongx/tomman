<span>{{ $odp->label }}</span>
@isset($odp->odc_id)
  <i class="fas fa-long-arrow-alt-left text-info"></i>
  <span class="label label-primary">ODC</span>
  <span>{{ $odp->odc_label }}</span>
@endisset
