@extends('app')

@section('title', 'MCore')

@section('style')
  <style>
    .px-content {
      padding: 0;
    }
    #loading-indicator {
      position: absolute;
      top: 20px;
      width: 100%;
      text-align: center;
    }
    #loading-indicator > span {
      padding: 4px 8px;
      background-color: #cc3e00;
    }
  </style>
@endsection

@section('body')
  <div id="mcore_map"></div>
  <div id="loading-indicator" class="hidden">
    <span>Loading...</span>
  </div>
@endsection

@section('script')
  @include('mcore.map.script')
  @include('mcore.map.const')
  @include('mcore.map.grid')
  @include('mcore.map.init')
  @include('mcore.map.features.sto')
  @include('mcore.map.features.grid')
  {{--@include('mcore.map.features.gridTest')--}}
  @include('partial.googlemap', ['callback' => 'McoreMap.init'])
@endsection
