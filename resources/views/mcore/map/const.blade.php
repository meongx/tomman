<script>
  window.McoreMap.const = {
    defaultZoom: 5,

    center: {
      lat: {!! \App\Service\Mcore\Map::CENTER_LAT !!},
      lng: {!! \App\Service\Mcore\Map::CENTER_LNG !!}
    },

    grids: {!! json_encode(\App\Service\Mcore\Map::GRID) !!},

    enums: {
      alproTypes: {!! json_encode(\App\Service\Mcore\Alpro::TYPES) !!}
    },

    types: {
      sto: '{!! \App\Service\Mcore\Helper::TYPE_STO !!}',
      odc: '{!! \App\Service\Mcore\Helper::TYPE_ODC !!}',
      odp: '{!! \App\Service\Mcore\Helper::TYPE_ODP !!}',
      pelanggan: '{!! \App\Service\Mcore\Helper::TYPE_PELANGGAN !!}',
      alpro: '{!! \App\Service\Mcore\Helper::TYPE_ALPRO !!}'
    },

    marker: {
      alpro: () => {
        return {
          url: '/img/mcore/alpro.png',
          size: new google.maps.Size(28, 58),
          origin: new google.maps.Point(0,0),
          anchor: new google.maps.Point(9,60)
        };
      }
    }
  };
</script>
