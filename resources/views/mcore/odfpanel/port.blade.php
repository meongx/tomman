<span class="{{ $portData->rear ? 'text-muted' : '' }}">{{ $portData->port }}</span>
@if ($portData->rear)
  <i class="fas fa-long-arrow-alt-right text-success"></i>

  @if ($portData->rear->dst_type == 'odc')
    <span class="label label-primary">ODC</span>
    <span class="text-muted">{{ $portData->rear->odc_label }}</span>
  @endif
@endif
