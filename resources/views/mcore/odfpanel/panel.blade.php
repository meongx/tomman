<div id="ports" class="overflow-h">
  <div class="port-container flex-{{ $panelData->slotvertical ? 'h' : 'v' }}">
    @foreach($groupedPort as $ports)
      <div class="otb-slot flex-{{ $panelData->slotvertical ? 'v' : 'h' }}">
        @foreach($ports as $link)
          <div class="portgroup">
            <div class="rearport {{ $link->rear ? 'up' : '' }}" data-port="{{ $link->port }}">
              <i class="fas fa-sort-up"></i>

              @if ($link->rear)
                <div class="hidden">
                  <div class="link-view">
                    @if ($link->rear->dst_type == \App\Service\Mcore\Helper::TYPE_ODC)
                      <div class="m-b-1">
                        <span class="label label-primary">FEED</span>
                        <a href="/mcore/feeder/{{ $link->rear->feeder_id }}">
                          {{ $link->rear->feeder_label }}
                        </a>
                      </div>
                      <div class="m-b-1">
                        @include('mcore.link.partial.tubecore', \App\Service\Mcore\Helper::getCoreData($link->rear->med_val))
                      </div>

                      <div class="m-b-1 m-l-3 text-success"><i class="fas fa-long-arrow-alt-down"></i></div>

                      <div class="m-b-1">
                        <div class="label label-primary">ODC</div>
                        <a href="/mcore/odc/{{ $link->rear->odc_id }}">
                          {{ $link->rear->odc_label }}
                        </a>
                      </div>
                      <div class="m-b-1">
                        <?php $token = explode(':', $link->rear->dst_val) ?>
                        <span>Panel</span>
                        <span class="label label-outline label-info">{{ $token[0] }}</span>
                        <span>Port</span>
                        <span class="label label-outline label-info">{{ $token[1] }}</span>
                      </div>
                    @endif

                    <a href="/mcore/sto/{{ $stoData->id }}/room/{{ $roomData->id }}/odf/{{ $odfData->id }}/panel/{{ $panelData->id }}/port/{{ $link->port }}"
                       class="btn m-t-4">
                      <i class="fas fa-link"></i>
                      <span>Detail Sambungan</span>
                    </a>
                  </div>
                </div>
              @endif
            </div>
            <div class="port">
              <span>{{ $link->port }}</span>
              <!-- TODO: popover -->
            </div>
          </div>
        @endforeach
      </div>
    @endforeach
  </div>
</div>
