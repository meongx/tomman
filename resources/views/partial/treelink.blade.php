<li>
  <div>
    <a href="{{ $tree->url }}">
      <span>{{ $tree->label }}</span>
      @isset($tree->count)
        <span class="label label-pill">{{ $tree->count }}</span>
      @endisset
    </a>
  </div>

  @if (count($tree->children))
    <ul class="tree">
      @each('partial.treelink', $tree->children, 'tree')
    </ul>
  @endif
</li>
