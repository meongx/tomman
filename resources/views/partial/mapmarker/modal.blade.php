<div id="mapMarkerModal" class="modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button"><span>&times;</span></button>
        <h4 class="modal-title">
          <span id="mapMarkerModal_title"></span>
          (
          <span id="mapMarkerModal_latText">0</span>
          ,
          <span id="mapMarkerModal_lngText">0</span>
          )
        </h4>
      </div>

      <div class="modal-body">
        <div id="mapMarkerModal_mapView">Loading Map...</div>
      </div>

      <div id="mapMarkerModal_footer" class="modal-footer">
        <button class="btn btn-warning pull-left" data-dismiss="modal" type="reset">
          <i class="fas fa-ban"></i>
          <span>Batal</span>
        </button>
        <button style="margin-right: 20px" id="mapMarkerModal_gpsBtn" class="btn btn-default" type="button">
          <i class="fas fa-map-marker-alt"></i>
          <span>GPS</span>
        </button>
        <button id="mapMarkerModal_okBtn" class="btn btn-primary" type="button">
          <i class="fas fa-check"></i>
          <span>OK</span>
        </button>
      </div>
    </div>
  </div>
</div>

<script>
  window.MapMarkerModal = (() => {

    let isGmapLoaded = false;
    let canEdit = true;
    let callback = null;
    let map = null;
    let marker = null;

    const defaultCenterPoint = {
      lat: 0.9804109,
      lng: 114.0140993
    };
    let centerPoint = {
      lat: defaultCenterPoint.lat,
      lng: defaultCenterPoint.lng
    };

    const $modal = $('#mapMarkerModal');
    const $title = $('#mapMarkerModal_title');
    const $latText = $('#mapMarkerModal_latText');
    const $lngText = $('#mapMarkerModal_lngText');
    const $modalFooter = $('#mapMarkerModal_footer');
    const $gpsBtn = $('#mapMarkerModal_gpsBtn');
    const $okBtn = $('#mapMarkerModal_okBtn');
    const $mapView = $('#mapMarkerModal_mapView');
    const mapView = $mapView[0];

    const mapActionHandler = e => {
      $latText.text(e.latLng.lat());
      $lngText.text(e.latLng.lng());

      showMarker(e.latLng);
    };

    const showMarker = latLng => {
      if (marker) {
        marker.setVisible(true);
        marker.setPosition(latLng);
      }
      else {
        marker = new google.maps.Marker({
          position: latLng,
          map: map,
          draggable: canEdit
        });
        marker.addListener('dragend', mapActionHandler);
      }
    };

    const initMap = () => {
      const withMarker = centerPoint.lat !== defaultCenterPoint.lat || centerPoint.lng !== defaultCenterPoint.lng;
      const zoom = withMarker ? 17 : 5;

      if (map) {
        map.setZoom(zoom);
        map.panTo(centerPoint);
      }
      else {
        $mapView.css('height', window.innerHeight - 250);

        map = new google.maps.Map(mapView, {
          center: centerPoint,
          zoom: zoom,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          clickableIcons: false
        });

        if (canEdit) {
          google.maps.event.addListener(map, 'click', mapActionHandler);
        }
      }

      if (withMarker) {
        showMarker(centerPoint);

        $latText.text(centerPoint.lat);
        $lngText.text(centerPoint.lng);
      }
      else if (marker) {
        marker.setVisible(false);
      }
    };

    $modal.on('shown.bs.modal', () => {
      if (isGmapLoaded) initMap();
    });

    $gpsBtn.click(() => {
      navigator.geolocation.getCurrentPosition(
        gps => {
          var latlng = {
            lat: gps.coords.latitude,
            lng: gps.coords.longitude
          };

          map.setZoom(17);
          map.panTo(latlng);
          showMarker(latlng);

          $latText.text(latlng.lat);
          $lngText.text(latlng.lng);
        },
        err => {
          if (err.code === 1) alert('ERROR: GPS Tidak Aktif\n\nAktifkan GPS kemudian coba lagi');
          else alert('ERROR: ' + err.code + '\n' + err.message);
        },
        {
          enableHighAccuracy: true
        }
      );
    });

    $okBtn.click(() => {
      $modal.modal('hide');

      if (!callback) {
        throw new Error("MapMarkerModal.setCallback() hasn't been called");
      }

      const latLng = marker ? marker.getPosition() : new google.maps.LatLng(0,0);
      callback(latLng.lat(), latLng.lng());
    });

    return {
      setCallback: cb => callback = cb,

      setTitle: t => $title.text(t),

      setCanEdit: e => {
        canEdit = e;

        if (canEdit) {
          $modalFooter.show();
        }
        else {
          $modalFooter.hide();
        }
      },

      isOpen: () => ($modal.data('bs.modal') || { isShown: false }).isShown,
      open: (lat, lng, cb) => {
        if (!isNaN(lat) && lat !== 0) centerPoint.lat = lat;
        if (!isNaN(lng) && lng !== 0) centerPoint.lng = lng;
        if (cb) MapMarkerModal.setCallback(cb);

        $modal.modal('show');
      },

      onGmapLoaded: () => {
        isGmapLoaded = true;
        if (MapMarkerModal.isOpen()) initMap();
      }
    };
  })();
</script>

@include('partial.googlemap', ['callback' => 'MapMarkerModal.onGmapLoaded'])
