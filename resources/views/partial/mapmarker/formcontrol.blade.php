<?php $hasError = $errors->has($field) ?>
<fieldset class="form-group form-message-light {{ $hasError?'has-error':'' }}">
  <label for="input-nama">{{ $label }}</label>

  @if ($canEdit)
    <div class="input-group">
      <input id="input-{{ $field }}" name="{{ $field }}" class="form-control"
             value="{{ old($field, (isset($object) ? "{$object->lat},{$object->lng}" : '') ) }}"
      @isset($attributes)
        @foreach($attributes as $key => $val)
          @if ($val === true)
            {{ $key }}
          @else
            {{ $key }}="{{ $val }}"
          @endif
        @endforeach
      @endisset
      >
      <span class="input-group-btn">
        <button id="btnPick-{{ $field }}" class="btn btn-default" type="button">
          <i class="fas fa-map-marker-alt"></i>
          <span>Dari Peta</span>
        </button>
      </span>
    </div>
  @else
    <p class="form-control-static">
      <input id="input-coordinate" value="{{ $object->lat }}, {{ $object->lng }}" type="hidden" />
      <span>{{ $object->lat }}, {{ $object->lng }}</span>
      <button id="btnPick-{{ $field }}" class="btn btn-default btn-xs" type="button">
        <i class="fas fa-map-marker-alt"></i>
        <span>View Map</span>
      </button>
    </p>
  @endif

  @if ($hasError)
    @foreach($errors->get($field) as $errorText)
      <small class="form-message light">{{ $errorText }}</small>
    @endforeach
  @endif
</fieldset>
