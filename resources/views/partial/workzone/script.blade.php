<script>
  $(function() {
    const $inputWorkzoneId = $('#input-{{ $field }}');
    const $inputWorkzoneText = $('#input-{{ $displayField }}');

    const modalTitle = '{{ $title ?? 'Pilih Area Kerja (Workzone)' }}';

    const callback = (id, nama) => {
      $inputWorkzoneId.val(id);
      $inputWorkzoneText.val(nama);
    };

    WorkzoneModal.setTitle(modalTitle);
    WorkzoneModal.setCallback(callback);
    WorkzoneModal.setCanEdit({{ $canEdit ? 'true' : 'false' }});

    $('#btnPick-{{ $field }}').click(() => WorkzoneModal.open($inputWorkzoneId.val()));
  });
</script>
