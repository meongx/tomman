<?php $count = count($workzoneTree) + count($workzoneTree[0]->children) ?>
<?php $hasMultipleWorkzone = $count > 1 ?>
<?php $hasError = $errors->has($field) ?>
<fieldset class="form-group form-message-light {{ $hasError?'has-error':'' }}">
  <label for="input-{{ $displayField }}">{{ $label }}</label>

  @if ($canEdit && $hasMultipleWorkzone)
    <div class="input-group">
      <input id="input-{{ $field }}" name="{{ $field }}" type="hidden"
             value="{{ old($field, $object->$field ?? '') }}">
      <input id="input-{{ $displayField }}" name="{{ $displayField }}" class="form-control"
             value="{{ old($displayField, $object->$displayField ?? '') }}" readonly
        @isset($attributes)
          @foreach($attributes as $key => $val)
            @if ($val === true)
              {{ $key }}
            @else
              {{ $key }}="{{ $val }}"
            @endif
          @endforeach
        @endisset
      >
      <span class="input-group-btn">
        <button id="btnPick-{{ $field }}" class="btn btn-default" type="button">
          <i class="fas fa-ellipsis-v"></i>
          <span>Pilih</span>
        </button>
      </span>
    </div>
  @elseif ($canEdit)
    <input id="input-{{ $field }}" name="{{ $field }}" type="hidden" value="{{ $object->$field ?? '' }}">
    <input id="input-{{ $displayField }}" value="{{ $workzoneTree[0]->label }}" class="form-control" readonly>
  @else
    <p class="form-control-static">{{ $object->$displayField ?? $workzoneTree[0]->label }}</p>
  @endif

  @if ($hasError)
    @foreach($errors->get($field) as $errorText)
      <small class="form-message light">{{ $errorText }}</small>
    @endforeach
  @endif
</fieldset>
