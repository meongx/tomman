@if (Session::has('alerts'))
  <div id="container-alert">
    @foreach(Session::get('alerts') as $alert)
      <div class="alert alert-{{ $alert['type'] }} alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        @if ($alert['type'] === 'danger' || $alert['type'] === 'warning')
          <i class="fas fa-exclamation-triangle"></i>
        @else
          <i class="fas fa-exclamation-circle"></i>
        @endif

        {!! $alert['text'] !!}
      </div>
    @endforeach
  </div>
@endif
