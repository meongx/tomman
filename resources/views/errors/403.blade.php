<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <title>FORBIDDEN - TOMMAN</title>
  <style>
    body {
      background-color: #e46050;
      text-align: center;
      font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
      margin: 0;
      padding: 0;
    }

    h1 {
      color: white;
      margin: 60px 0 0;
      text-shadow: 0 4px 0 rgba(0,0,0,.1);
      font-size: 80px;
      font-weight: 700;
    }
    h2 {
      color: rgba(0,0,0,.5);
      margin-top: 0;
    }

    p {
      font-size: 22px;
    }

    .header {
      padding: 15px 0;
      font-size: 24px;
      font-weight: bold;
      background-color: #374049;
      box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
    }
    .header a {
      color: white;
      text-decoration: none;
    }
  </style>
</head>
<body>
<div class="header">
  <a href="/">TOMMANv2</a>
</div>
<h1>403</h1>
<h2>FORBIDDEN</h2>
<p>Anda tidak berhak mengakses data ini</p>
</body>
