<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <title>New User - TOMMAN</title>
  <style>
    body {
      background-color: #52bcdc;
      text-align: center;
      font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
      margin: 0;
      padding: 0;
    }

    h1 {
      color: white;
      margin-top: 60px;
      text-shadow: 0 4px 0 rgba(0,0,0,.1);
      font-size: 80px;
      font-weight: 700;
    }

    p {
      font-size: 22px;
    }

    .header {
      padding: 15px 0;
      font-size: 24px;
      font-weight: bold;
      color: white;
      background-color: #374049;
      box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
    }
  </style>
</head>
<body>
  <div class="header">TOMMANv2</div>
  <h1>USER BARU</h1>
  <p>Ini adalah pertama kali Anda mengakses TOMMAN</p>
  <p>silahkan hubungi <em>Ambassador</em> TOMMAN di wilayah kerja anda untuk melakukan aktivasi user</p>
</body>
