<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  <link rel="stylesheet" href="{{ _url('/css/vendor.css') }}">
  <link rel="stylesheet" href="{{ _url('/css/app.css') }}">

  <title>@yield('title', 'TOMMANv2')</title>

  @yield('style')
</head>
<body>
<nav class="px-nav px-nav-left" data-animate="0">
  <button class="px-nav-toggle visible-xs-block visible-md-block" data-toggle="px-nav" type="button">
    <span class="px-nav-toggle-arrow"></span>
    <span class="navbar-toggle-icon"></span>
    <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
  </button>

  <ul class="px-nav-content">
    <?php
      $activeParentIndex = -1;
      $activeIndex = -1;
      $currentUrl = Request::path().'/';

      foreach ($mainMenu as $parentIndex => $menu) {
        foreach($menu->submenu as $index => $entry) {
          $entryUrl = substr($entry->url, 1) . '/';

          if ($entryUrl == substr($currentUrl, 0, strlen($entryUrl))) {
            $activeParentIndex = $parentIndex;
            $activeIndex = $index;
          }
        }
      }
    ?>

    @foreach($mainMenu as $parentIndex => $menu)
      <li class="px-nav-item {{ isset($menu->submenu) ? 'px-nav-dropdown' : '' }}">
        <a href="{{ isset($menu->url) ? $menu->url : '#' }}">
          <i class="px-nav-icon {{ $menu->class }}"></i>
          <span class="px-nav-label">{{ $menu->label }}</span>
        </a>

        <ul class="px-nav-dropdown-menu">
          @foreach($menu->submenu as $index => $entry)
            <?php $isActiveUrl = ($parentIndex == $activeParentIndex && $index == $activeIndex) ?>
            <li class="px-nav-item {{ $isActiveUrl ? 'active' : '' }}">
              <a href="{{ $entry->url }}">
                <span class="px-nav-label">{{ $entry->label }}</span>
              </a>
            </li>
          @endforeach
        </ul>
      </li>
    @endforeach

    <li class="px-nav-item">
      <a href="/logout">
        <i class="px-nav-icon fas fa-sign-out-alt"></i>
        <span class="px-nav-label">Logout</span>
      </a>
    </li>
  </ul>
</nav>

<nav class="navbar px-navbar hidden-md">
  <div class="navbar-header">
    <a href="/" class="navbar-brand">TOMMANv2</a>
  </div>
</nav>

<div class="px-content">
  @include('partial.alerts')

  @yield('body')
</div>

<footer class="px-footer px-footer-bottom">
  Copyright &copy;
</footer>

<script src="{{ _url('/js/vendor.js') }}"></script>
<script>
  $.ajaxSetup({
    beforeSend: function (xhr, settings) {
      if (
        (settings.type === 'POST' || settings.type === 'PUT' || settings.type === 'DELETE')
        &&
        // (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url)))
        // (!(settings.url.substr(0,7) === 'http://' || settings.url.substr(0,8) === 'https://'))
        !(/^https?:\/\//.test(settings.url))
      ) {
        xhr.setRequestHeader('X-CSRF-TOKEN', '{!! csrf_token() !!}');
      }
    }
  });

  $('.px-nav').pxNav();
</script>

@yield('script')

</body>
</html>
