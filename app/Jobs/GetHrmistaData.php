<?php

namespace App\Jobs;

use App\Service\PortalTA\Hrmista;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Service\Auth\CredentialCache;

class GetHrmistaData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    private $login;

    /**
     * Create a new job instance.
     *
     * @param string $login
     *
     * @return void
     */
    public function __construct($login)
    {
        $this->login = $login;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $login = $this->login;
        $password = CredentialCache::retrieve($login);

        $hrData = Hrmista::retrieve($login, $password);
        if ($hrData) {
            $this->release(3);
        }

        // TODO: save data
    }
}
