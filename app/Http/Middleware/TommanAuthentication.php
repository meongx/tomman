<?php

namespace App\Http\Middleware;

use App\Service\Auth\CredentialCache;
use Closure;
use App\Service\SessionHelper;
use App\Service\Auth\User;

class TommanAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (SessionHelper::hasCurrentUser()) {
            return $next($request);
        }

        $rememberToken = $request->cookie(SessionHelper::KEY_PERSISTENT_COOKIE);
        if ($rememberToken) {
            $user = User::getByRememberToken($rememberToken);
            if ($user) {
                $isLocalUser = $user->type == User::TYPE_LOCALUSER;
                $hasCachedCredential = CredentialCache::isCached($user->login);

                if ($isLocalUser || $hasCachedCredential) {
                    SessionHelper::setCurrentUser($user);
                }

                return $next($request);
            }
        }

        // TODO: remember POST input
        SessionHelper::setLoginRedirect($request->fullUrl());
        // TODO: test fetch/ServiceWorker
        if ($request->ajax()) {
            return response('UNAUTHORIZED', 401);
        } else {
            return redirect('login');
        }
    }
}
