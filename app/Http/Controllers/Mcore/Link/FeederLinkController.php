<?php

namespace App\Http\Controllers\Mcore\Link;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\Link\SubmitOdfOdc;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Mcore\Helper as Mcore;
use App\Service\Mcore\OdcCached;
use App\Service\Mcore\OdfPanelCached;
use App\Service\Mcore\Link\FeederLinkCached;

class FeederLinkController extends Controller
{
    public function odcForm($id, $panel, $port)
    {
        [$odcData, $odc_mtime] = OdcCached::getById($id);
        if (!$odcData) {
            abort(404);
        }

        [$linkData, $link_mtime] = FeederLinkCached::getByOdcPanelPort($id, $panel, $port);
        if (!$linkData) {
            $linkData = (object)[
                'dst_type' => 'odc',
                'dst_id' => $id,
                'dst_val' => "$panel:$port:rear",

                'odc_id' => $odcData->id,
                'odc_label' => $odcData->label,
                'odc_workzone_id' => $odcData->workzone_id,
                'odc_workzone_label' => $odcData->workzone_label,

                'odc_panel' => $panel,
                'odc_port' => $port
            ];
        } else {
            $token = explode(':', $linkData->src_val);
            $linkData->odf_panel_port = $token[0];

            $token = explode(':', $linkData->dst_val);
            $linkData->odc_panel = $token[0];
            $linkData->odc_port = $token[1];
        }

        $canEdit = $this->getCanEdit($odcData->workzone_path);

        return view('mcore.link.otb-odc', compact('linkData', 'canEdit'));
    }

    public function otbForm($stoId, $roomId, $odfId, $otbId, $port)
    {
        [$otbData, $otb_mtime] = OdfPanelCached::getById($otbId);
        if (!$otbData) {
            abort(404);
        }

        [$linkData, $link_mtime] = FeederLinkCached::getByOtbPort($otbId, $port);
        if (!$linkData) {
            $linkData = (object)[
                'src_type' => Mcore::TYPE_ODF_PANEL,
                'src_id' => $otbId,
                'src_val' => "$port:rear",
                'odf_panel_port' => $port,

                'odf_panel_id' => $otbData->id,
                'odf_panel_label' => $otbData->label,
                'odf_id' => $otbData->odf_id,
                'odf_label' => $otbData->odf_label,
                'sto_room_id' => $otbData->sto_room_id,
                'sto_room_label' => $otbData->sto_room_label,
                'sto_id' => $otbData->sto_id,
                'sto_label' => $otbData->sto_label,
                'sto_workzone_id' => $otbData->sto_workzone_id,
                'sto_workzone_label' => $otbData->sto_workzone_label
            ];
        } else {
            $odfToken = explode(':', $linkData->src_val);
            $linkData->odf_panel_port = $odfToken[0];

            $odcToken = explode(':', $linkData->dst_val);
            $linkData->odc_panel = $odcToken[0];
            $linkData->odc_port = $odcToken[1];
        }

        $canEdit = $this->getCanEdit($otbData->sto_workzone_path);

        return view('mcore.link.otb-odc', compact('linkData', 'canEdit'));
    }

    public function submit(SubmitOdfOdc $request)
    {
        return ($request->action === 'plug')
            ? $this->plug($request)
            : $this->unplug($request)
        ;
    }

    private function plug(Request $request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            FeederLinkCached::plug(
                $user->id,
                $request->src_id,
                $request->src_val,
                $request->med_id,
                $request->med_val,
                $request->dst_id,
                $request->dst_val
            );

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menyimpan data Sambungan'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menyimpan data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    private function unplug(Request $request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            FeederLinkCached::unplug(
                $user->id,
                $request->src_id,
                $request->src_val,
                $request->med_id,
                $request->med_val,
                $request->dst_id,
                $request->dst_val
            );

            $requestPath = $request->path();
            $findstr = $request->is('*/odc/*') ? '/panel/' : '/port/'; // TODO: ODF side url
            $url = substr($requestPath, 0, strpos($requestPath, $findstr));

            return redirect($url)->with('alerts', [
                [
                    'type' => 'warning',
                    'text' => '<strong>Sukses</strong> mencabut data Sambungan'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menyimpan data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    private function getCanEdit($workzonePath)
    {
        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission(
            'mcore.link.odc-odp',
            Authorization::WRITE
        );
        $isWithinZone = Authorization::isWithinZone(
            $currentUser->workzone_path,
            $workzonePath
        );

        return $hasWriteAccess && $isWithinZone;
    }
}
