<?php

namespace App\Http\Controllers\Mcore\Link;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\Link\SubmitOdcOdp;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\Helper as McoreHelper;
use App\Service\Mcore\OdcCached;
use App\Service\Mcore\OdpCached;
use App\Service\Mcore\DistribusiCached;
use App\Service\Mcore\Link\OdcToOdpCached;

class OdcOdpLinkController extends Controller
{
    public function odcForm($id, $panel, $port)
    {
        [$odcData, $odc_mtime] = OdcCached::getById($id);
        if (!$odcData) {
            abort(404);
        }

        [$linkData, $link_mtime] = OdcToOdpCached::getByOdcPanelPort($id, $panel, $port);
        if (!$linkData) {
            $linkData = (object)[
                'src_type' => 'odc',
                'src_id' => $id,
                'src_val' => "$panel:$port:rear",

                'odc_id' => $odcData->id,
                'odc_label' => $odcData->label,
                'odc_workzone_id' => $odcData->workzone_id,
                'odc_workzone_label' => $odcData->workzone_label,

                'odc_panel' => $panel,
                'odc_port' => $port
            ];
        } else {
            $token = explode(':', $linkData->src_val);
            $linkData->odc_panel = $token[0];
            $linkData->odc_port = $token[1];
        }

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission(
            'mcore.link.odc-odp',
            Authorization::WRITE
        );
        $isWithinZone = Authorization::isWithinZone(
            $currentUser->workzone_path,
            $odcData->workzone_path
        );
        $canEdit = $hasWriteAccess && $isWithinZone;

        return view('mcore.link.odc-odp', compact('linkData', 'canEdit'));
    }

    public function odpForm($id)
    {
        [$odpData, $odp_mtime] = OdpCached::getById($id);
        if (!$odpData) {
            abort(404);
        }

        [$linkData, $link_mtime] = OdcToOdpCached::getByOdp($id);
        if (!$linkData) {
            $linkData = (object)[
                'dist_type' => 'odc',
                'dist_id' => $id,

                'odp_id' => $odpData->id,
                'odp_label' => $odpData->label,
                'odp_workzone_id' => $odpData->workzone_id,
                'odp_workzone_label' => $odpData->workzone_label
            ];
        } else {
            $token = explode(':', $linkData->src_val);
            $linkData->odc_panel = $token[0];
            $linkData->odc_port = $token[1];
        }

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.link.odc-odp', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $odpData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        return view('mcore.link.odc-odp', compact('linkData', 'canEdit'));
    }

    public function distribusiByWorkzoneAsSelect2(Request $request, $workzoneId)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($workzoneId);
        [$distribusiList, $dist_mtime] = DistribusiCached::paginateByWorkzonePath(
            $workzoneData->path,
            $page,
            $search
        );

        $result = [];
        foreach ($distribusiList->all() as $distribusiData) {
            $distribusiData->text = $distribusiData->label;

            $result[] = $distribusiData;
        }

        return [
            'results' => $result,
            'paginate' => [
                'more' => $distribusiList->hasMorePages()
            ]
        ];
    }

    public function coreByDistribusiAsSelect2($distribusiId)
    {
        $corePerTube = McoreHelper::CORE_PER_TUBE;
        [$distribusiData, $dist_mtime] = DistribusiCached::getById($distribusiId);

        [$linkList, $_] = OdcToOdpCached::getByDistribusi($distribusiId);
        $coreList = DistribusiCached::linksToCores($distribusiData->capacity, $linkList->all());

        $tubeView = View::make('mcore.link.partial.tube');
        $coreView = View::make('mcore.link.partial.core');

        $result = [];
        foreach ($coreList as $tube) {
            $selectGroup = (object)[
                'text' => $tubeView->with(compact('tube'))->render(),
                'children' => []
            ];

            foreach ($tube->coreList as $core) {
                $coreNum = $core->coreNum;
                $tubeNum = $tube->tubeNum;
                $coreId = $core->coreNum + (($tubeNum - 1) * $corePerTube);
                $link = $core->link ?: null;

                $selectGroup->children[] = (object)[
                    'id' => $coreId,
                    'text' => $coreView->with(compact('coreId', 'tubeNum', 'coreNum', 'link'))->render(),
                    'disabled' => $core->link ? true : false
                ];
            }
            $result[] = $selectGroup;
        }

        return $result;
    }

    public function odpByWorkzoneAsSelect2(Request $request, $workzoneId)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($workzoneId);
        [$odpList, $odp_mtime] = OdpCached::paginateByWorkzonePathWithSourceLink($workzoneData->path, $page, $search);

        $odpView = View::make('mcore.link.partial.odp');
        $result = [];
        foreach ($odpList as $odp) {
            $odp->text = $odpView->with(compact('odp'))->render();
            if ($odp->odc_id) {
                $odp->disabled = true;
            }

            $result[] = $odp;
        }

        return [
            'results' => $result,
            'paginate' => [
                'more' => $odpList->hasMorePages()
            ]
        ];
    }

    public function submit(SubmitOdcOdp $request)
    {
        return ($request->action === 'plug')
            ? $this->plug($request)
            : $this->unplug($request)
        ;
    }

    private function plug($request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            OdcToOdpCached::plug(
                $user->id,
                $request->src_id,
                $request->src_val,
                $request->med_id,
                $request->med_val,
                $request->dst_id
            );

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menyimpan data Sambungan'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menyimpan data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    private function unplug($request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            OdcToOdpCached::unplug(
                $user->id,
                $request->src_id,
                $request->src_val,
                $request->med_id,
                $request->med_val,
                $request->dst_id
            );

            $requestPath = $request->path();
            $findstr = $request->is('*/odc/*') ? '/panel/' : '/dist';
            $url = substr($requestPath, 0, strpos($requestPath, $findstr));

            return redirect($url)->with('alerts', [
                [
                    'type' => 'warning',
                    'text' => '<strong>Sukses</strong> mencabut data Sambungan'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menyimpan data<br>'.$e->getMessage()
                ]
            ]);
        }
    }
}
