<?php

namespace App\Http\Controllers\Mcore;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\SubmitOdp;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\OdpCached;
use App\Service\Mcore\Link\OdcToOdpCached;
use App\Service\Mcore\Link\OdpToOntCached;

class OdpController extends Controller
{
    public function index()
    {
        $currentUser = SessionHelper::getCurrentUser();
        $canCreateNew = SessionHelper::currentUserHasPermission('mcore.odp', Authorization::WRITE);
        [$workzoneTree, $wz_mtime] = OdpCached::countByWorkzonePath($currentUser->workzone_path);

        $count = count($workzoneTree) + count($workzoneTree[0]->children);
        if ($count == 1) {
            return redirect('/mcore/odp/workzone/'.$workzoneTree[0]->id);
        }

        return view('mcore.odp.index', compact('canCreateNew', 'workzoneTree'));
    }

    public function listByWorkzone(Request $request, $id)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($id);
        [$odpList, $odp_mtime] = OdpCached::paginateByWorkzonePath($workzoneData->path, $page, $search);
        $canCreateNew = SessionHelper::currentUserHasPermission('mcore.odc', Authorization::WRITE);

        return view('mcore.odp.list', compact('workzoneData', 'odpList', 'canCreateNew'));
    }

    public function listByWorkzoneAsSelect2(Request $request, $id)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($id);
        [$odpList, $odp_mtime] = OdpCached::paginateByWorkzonePath($workzoneData->path, $page, $search);

        $result = [];
        foreach ($odpList->all() as $odpData) {
            $odpData->text = $odpData->label;

            $result[] = $odpData;
        }

        return [
            'results' => $result,
            'paginate' => [
                'more' => $odpList->hasMorePages()
            ]
        ];
    }

    public function createForm()
    {
        $currentUser = SessionHelper::getCurrentUser();
        [$workzoneTree, $wz_mtime] = WorkzoneCached::getByPath($currentUser->workzone_path);
        $canEdit = true;

        $statuses = OdpCached::STATUSES;
        $types = OdpCached::TYPES;
        $capacities = OdpCached::CAPACITIES;

        return view(
            'mcore.odp.form',
            compact('workzoneTree', 'canEdit', 'statuses', 'types', 'capacities')
        );
    }

    public function create(SubmitOdp $request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            $id = OdpCached::create(
                $user->id,
                $request->workzone_id,
                $request->label,
                $request->tenoss,
                $request->type,
                $request->status,
                $request->capacity,
                $request->lat,
                $request->lng
            );
            $link = '<a href="/mcore/odp/'.$id.'">'.$request->label.'</a>';

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menambah data ODP '.$link
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function updateForm($id)
    {
        [$odpData, $odp_mtime] = OdpCached::getById($id);
        if (!$odpData) {
            abort(404);
        }

        [$sourceLink, $source_mtime] = OdcToOdpCached::getByOdp($id);
        [$links, $links_mtime] = OdpToOntCached::getByOdp($odpData->id);
        $ports = OdpCached::linksToPorts($odpData->capacity, $links->all());

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.odp', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $odpData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        [$workzoneTree, $workzoneLastModified] = WorkzoneCached::getByPath($currentUser->workzone_path);

        $statuses = OdpCached::STATUSES;
        $types = OdpCached::TYPES;
        $capacities = OdpCached::CAPACITIES;

        return view(
            'mcore.odp.form',
            compact(
                'odpData',
                'workzoneTree',
                'canEdit',
                'statuses',
                'types',
                'capacities',
                'ports',
                'sourceLink'
            )
        );
    }

    public function update(SubmitOdp $request, $id)
    {
        [$odpData, $odp_mtime] = OdpCached::getById($id);
        if (!$odpData) {
            abort(404);
        }

        try {
            $user = SessionHelper::getCurrentUser();
            OdpCached::update(
                $user->id,
                $id,
                $request->workzone_id,
                $request->label,
                $request->tenoss,
                $request->type,
                $request->status,
                $request->capacity,
                $request->lat,
                $request->lng
            );

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> merubah data ODP'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }
}
