<?php

namespace App\Http\Controllers\Mcore;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Requests\Mcore\SubmitOdc;
use App\Http\Controllers\Controller;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\OdcCached;
use App\Service\Mcore\OdcSplitterCached;
use App\Service\Mcore\Link\OdcSplitterCached as OdcSplitterLinkCached;
use App\Service\Mcore\Link\FeederLinkCached;

class OdcController extends Controller
{
    public function index()
    {
        $currentUser = SessionHelper::getCurrentUser();
        $canCreateNew = SessionHelper::currentUserHasPermission('mcore.odc', Authorization::WRITE);
        [$workzoneTree, $wz_mtime] = OdcCached::countByWorkzonePath($currentUser->workzone_path);

        $count = count($workzoneTree) + count($workzoneTree[0]->children);
        if ($count == 1) {
            return redirect('/mcore/odc/workzone/'.$workzoneTree[0]->id);
        }

        return view('mcore.odc.index', compact('canCreateNew', 'workzoneTree'));
    }

    public function rearLink($id, $panel, $port)
    {
        $url = "/mcore/odc/$id/panel/$panel/port/$port/";
        $feederLink = FeederLinkCached::getByOdcPanelPort($id, $panel, $port)[0];

        $url .= $feederLink ? 'odf' : 'odp';
        return redirect($url);
    }

    public function listByWorkzone(Request $request, $id)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($id);
        [$odcList, $odc_mtime] = OdcCached::paginateByWorkzonePath($workzoneData->path, $page, $search);
        $canCreateNew = SessionHelper::currentUserHasPermission('mcore.odc', Authorization::WRITE);

        return view('mcore.odc.list', compact('workzoneData', 'odcList', 'canCreateNew'));
    }

    public function listByWorkzoneAsSelect2(Request $request, $id)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($id);
        [$odcList, $odc_mtime] = OdcCached::paginateByWorkzonePath($workzoneData->path, $page, $search);

        $result = [];
        foreach ($odcList->all() as $odcData) {
            $odcData->text = $odcData->label;

            $result[] = $odcData;
        }

        return [
            'results' => $result,
            'paginate' => [
                'more' => $odcList->hasMorePages()
            ]
        ];
    }

    public function rearPortAsSelect2($id)
    {
        [$odcData, $odc_mtime] = OdcCached::getById($id);
        if (!$odcData) {
            abort(404);
        }

        $splitterList = [];
        [$linkList, $links_mtime] = OdcCached::getLinks($id);
        $panelPorts = OdcCached::parsePanelPort($odcData, $splitterList, $linkList->all());

        $result = [];
        $portView = View::make('mcore.odc.partial.rearport');
        foreach ($panelPorts as $panel => $ports) {
            $portList = [];
            foreach ($ports as $portData) {
                $portData->panel = $panel;

                $portList[] = (object)[
                    'id' => "$panel:{$portData->port}:rear",
                    'text' => $portView->with(compact('portData'))->render(),
                    'disabled' => $portData->rear ? true : false
                ];
            }

            $result[] = (object)[
                'text' => 'Panel '.$panel,
                'children' => $portList
            ];
        }

        return $result;
    }

    public function createForm()
    {
        $currentUser = SessionHelper::getCurrentUser();
        [$workzoneTree, $wz_mtime] = WorkzoneCached::getByPath($currentUser->workzone_path);
        $canEdit = true;
        $ports = OdcCached::PORT_COUNTS;
        $types = OdcCached::TYPES;

        return view('mcore.odc.form', compact('workzoneTree', 'canEdit', 'ports', 'types'));
    }

    public function create(SubmitOdc $request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            $id = OdcCached::create(
                $user->id,
                $request->workzone_id,
                $request->label,
                $request->panelcount,
                $request->portperpanel,
                $request->type,
                $request->lat,
                $request->lng
            );
            $link = '<a href="/mcore/odc/'.$id.'">'.$request->label.'</a>';

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menambah data ODC '.$link
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function updateForm($id)
    {
        [$odcData, $odc_mtime] = OdcCached::getById($id);
        if (!$odcData) {
            abort(404);
        }

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.odc', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $odcData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        [$workzoneTree, $workzoneLastModified] = WorkzoneCached::getByPath($currentUser->workzone_path);

        [$splitterList, $splitters_mtime] = OdcSplitterCached::getByOdc($id);
        [$linkList, $links_mtime] = OdcCached::getLinks($id);
        $panelPorts = OdcCached::parsePanelPort($odcData, $splitterList, $linkList->all());

        $ports = OdcCached::PORT_COUNTS;
        $types = OdcCached::TYPES;

        return view(
            'mcore.odc.form',
            compact('odcData', 'workzoneTree', 'splitterList', 'canEdit', 'ports', 'types', 'panelPorts')
        );
    }

    public function update(SubmitOdc $request, $id)
    {
        [$odcData, $odc_mtime] = OdcCached::getById($id);
        if (!$odcData) {
            abort(404);
        }

        try {
            $user = SessionHelper::getCurrentUser();
            OdcCached::update(
                $user->id,
                $id,
                $request->workzone_id,
                $request->label,
                $request->panelcount,
                $request->portperpanel,
                $request->type,
                $request->lat,
                $request->lng
            );

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> merubah data ODC'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function submitSplitter(Request $request, $id)
    {
        [$odcData, $odc_mtime] = OdcCached::getById($id);
        if (!$odcData) {
            abort(404);
        }

        if ($request->splitter_new) {
            return $this->createAndPlugSplitter(
                $odcData->id,
                $request->odc_panel,
                $request->odc_port,
                $request->splitter_label,
                $request->splitter_port
            );
        } elseif ($request->unplug) {
            return $this->unplugSplitter(
                $id,
                $request->odc_panel,
                $request->odc_port
            );
        } elseif (isset($request->splitter_port)) {
            return $this->plugSplitter(
                $id,
                $request->odc_panel,
                $request->odc_port,
                $request->splitter_id,
                $request->splitter_port
            );
        } elseif ($request->splitter_id) {
            return $this->updateSplitter(
                $odcData->id,
                $request->splitter_id,
                $request->splitter_label
            );
        } else {
            return abort(400);
        }
    }

    private function createAndPlugSplitter($odcId, $odcPanel, $odcPort, $splitterLabel, $splitterPort)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            OdcSplitterCached::create(
                $user->id,
                $odcId,
                $splitterLabel,
                $odcPanel,
                $odcPort,
                $splitterPort
            );

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menambah data Splitter'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    private function unplugSplitter($odcId, $odcPanel, $odcPort)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            OdcSplitterLinkCached::unplugByOdc($user->id, $odcId, $odcPanel, $odcPort);

            return back()->with('alerts', [
                [
                    'type' => 'warning',
                    'text' => '<strong>Sukses</strong> mencabut sambungan'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> mencabut sambungan<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    private function plugSplitter($odcId, $odcPanel, $odcPort, $splitterId, $splitterPort)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            OdcSplitterLinkCached::plug(
                $user->id,
                $odcId,
                $odcPanel,
                $odcPort,
                $splitterId,
                $splitterPort
            );

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menyimpan sambungan'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah sambungan<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    private function updateSplitter($odcId, $splitterId, $splitterLabel)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            OdcSplitterCached::update($user->id, $odcId, $splitterId, $splitterLabel);

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> merubah data Splitter'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }
}
