<?php

namespace App\Http\Controllers\Mcore;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\SubmitSto;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\StoCached;
use App\Service\Mcore\StoRoomCached;

class StoController extends Controller
{
    public function index()
    {
        $currentUser = SessionHelper::getCurrentUser();
        $canCreateNew = SessionHelper::currentUserHasPermission('mcore.sto', Authorization::WRITE);
        [$workzoneTree, $_mtime] = StoCached::countByWorkzonePath($currentUser->workzone_path);

        $count = count($workzoneTree) + count($workzoneTree[0]->children);
        if ($count == 1) {
            return redirect('/mcore/odc/workzone/'.$workzoneTree[0]->id);
        }

        return view('mcore.sto.index', compact('canCreateNew', 'workzoneTree'));
    }

    public function listByWorkzone(Request $request, $id)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $workzone_mtime] = WorkzoneCached::getById($id);
        [$stoList, $sto_mtime] = StoCached::paginateByWorkzonePath($workzoneData->path, $page, $search);
        $canCreateNew = SessionHelper::currentUserHasPermission('mcore.sto', Authorization::WRITE);

        return view('mcore.sto.list', compact('workzoneData', 'stoList', 'canCreateNew'));
    }

    public function listByWorkzoneAsSelect2(Request $request, $id)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $workzone_mtime] = WorkzoneCached::getById($id);
        [$stoList, $sto_mtime] = StoCached::paginateByWorkzonePath($workzoneData->path, $page, $search);

        $result = [];
        foreach ($stoList->all() as $stoData) {
            $stoData->text = $stoData->label;

            $result[] = $stoData;
        }

        return [
            'results' => $result,
            'paginate' => [
                'more' => $stoList->hasMorePages()
            ]
        ];
    }

    public function listRoomByStoAsSelect2($id)
    {
        [$roomList, $room_mtime] = StoRoomCached::listBySto($id);

        $result = [];
        foreach ($roomList->all() as $roomData) {
            $roomData->text = $roomData->label;

            $result[] = $roomData;
        }

        return $result;
    }

    public function createForm()
    {
        $stoData = null;
        // TODO: pre-select workzone when added from sto/workzone/id page
        $currentUser = SessionHelper::getCurrentUser();
        [$workzoneTree, $workzone_mtime] = WorkzoneCached::getByPath($currentUser->workzone_path);
        $canEdit = true;

        return view('mcore.sto.form', compact('stoData', 'workzoneTree', 'canEdit'));
    }

    public function create(SubmitSto $request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            $id = StoCached::create($user->id, $request->workzone_id, $request->label, $request->lat, $request->lng);
            $link = '<a href="/mcore/sto/'.$id.'">'.$request->label.'</a>';

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menambah data STO '.$link
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function updateForm($id)
    {
        [$stoData, $sto_mtime] = StoCached::getById($id);
        if (!$stoData) {
            abort(404);
        }

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.sto', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $stoData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;
        $stoData->last_operation->datetime->setTimezone($currentUser->phpTimezone);
        [$roomList, $room_mtime] = StoRoomCached::listBySto($id);

        [$workzoneTree, $workzone_mtime] = WorkzoneCached::getByPath($currentUser->workzone_path);

        return view('mcore.sto.form', compact('stoData', 'workzoneTree', 'canEdit', 'roomList'));
    }

    public function update(SubmitSto $request, $id)
    {
        [$stoData, $sto_mtime] = StoCached::getById($id);
        if (!$stoData) {
            abort(404);
        }

        try {
            $user = SessionHelper::getCurrentUser();
            StoCached::update($user->id, $id, $request->workzone_id, $request->label, $request->lat, $request->lng);

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> merubah data STO '
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    // TODO: paginate history
    public function historyList($id)
    {
        [$stoData, $mtime] = StoCached::getById($id);
        $historyList = StoCached::allHistoryById($id);
        $timezone = SessionHelper::getCurrentUser()->phpTimezone;

        // TODO: build history graph

        return view('mcore.sto.audit', compact('stoData', 'historyList', 'timezone'));
    }
}
