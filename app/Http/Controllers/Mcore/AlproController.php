<?php

namespace App\Http\Controllers\Mcore;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\SubmitAlpro;
use App\Service\SessionHelper;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\Alpro;

class AlproController extends Controller
{
    public function inputForm()
    {
        $currentUser = SessionHelper::getCurrentUser();
        [$workzoneTree, $wz_mtime] = WorkzoneCached::getByPath($currentUser->workzone_path);
        $types = Alpro::TYPES;

        return view('mcore.alpro.map', compact('workzoneTree', 'types'));
    }

    public function create(SubmitAlpro $request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            Alpro::create($user->id, $request->workzone_id, $request->type, $request->lat, $request->lng);

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menambah data '.Alpro::TYPES[$request->type]
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }
}
