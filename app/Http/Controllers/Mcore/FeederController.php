<?php

namespace App\Http\Controllers\Mcore;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\SubmitFeeder;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\Helper as Mcore;
use App\Service\Mcore\Map;
use App\Service\Mcore\Alpro;
use App\Service\Mcore\FeederCached;
use App\Service\Mcore\Link\FeederLinkCached;

class FeederController extends Controller
{
    const KEY_RETURN_URL = 'returnUrl';

    public function index()
    {
        $currentUser = SessionHelper::getCurrentUser();
        $canCreateNew = SessionHelper::currentUserHasPermission(
            'mcore.feeder',
            Authorization::WRITE
        );
        [$workzoneTree, $wz_mtime] = FeederCached::countByWorkzonePath($currentUser->workzone_path);

        $count = count($workzoneTree) + count($workzoneTree[0]->children);
        if ($count == 1) {
            return redirect('/mcore/feeder/workzone/'.$workzoneTree[0]->id);
        }

        return view('mcore.feeder.index', compact('canCreateNew', 'workzoneTree'));
    }

    public function listByWorkzone(Request $request, $id)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($id);
        [$feederList, $dist_mtime] = FeederCached::paginateByWorkzonePath($workzoneData->path, $page, $search);
        $canCreateNew = SessionHelper::currentUserHasPermission(
            'mcore.feeder',
            Authorization::WRITE
        );

        return view('mcore.feeder.list', compact('workzoneData', 'feederList', 'canCreateNew'));
    }

    public function listByWorkzoneAsSelect2(Request $request, $workzoneId)
    {
        $page = $request->query('page', 1);
        $search = $request->query('q');

        [$workzoneData, $wz_mtime] = WorkzoneCached::getById($workzoneId);
        [$feederList, $dist_mtime] = FeederCached::paginateByWorkzonePath(
            $workzoneData->path,
            $page,
            $search
        );

        $result = [];
        foreach ($feederList->all() as $feederData) {
            $feederData->text = $feederData->label;

            $result[] = $feederData;
        }

        return [
            'results' => $result,
            'paginate' => [
                'more' => $feederList->hasMorePages()
            ]
        ];
    }

    public function coreByIdAsSelect2($id)
    {
        $corePerTube = Mcore::CORE_PER_TUBE;
        [$feederData, $feed_mtime] = FeederCached::getById($id);

        [$linkList, $link_mtime] = FeederLinkCached::getByFeeder($id);
        $coreList = FeederCached::linksToCores($feederData->capacity, $linkList->all());

        $tubeView = View::make('mcore.link.partial.tube');
        $coreView = View::make('mcore.link.partial.core');

        $result = [];
        foreach ($coreList as $tube) {
            $selectGroup = (object)[
                'text' => $tubeView->with(compact('tube'))->render(),
                'children' => []
            ];

            foreach ($tube->coreList as $core) {
                $coreNum = $core->coreNum;
                $tubeNum = $tube->tubeNum;
                $coreId = $core->coreNum + (($tubeNum - 1) * $corePerTube);
                $link = $core->link ?: null;

                $selectGroup->children[] = (object)[
                    'id' => $coreId,
                    'text' => $coreView->with(compact('coreId', 'tubeNum', 'coreNum', 'link'))->render(),
                    'disabled' => $core->link ? true : false
                ];
            }
            $result[] = $selectGroup;
        }

        return $result;
    }

    public function createForm(Request $request)
    {
        $currentUser = SessionHelper::getCurrentUser();
        [$workzoneTree, $wz_mtime] = WorkzoneCached::getByPath($currentUser->workzone_path);

        $feederData = null;
        $refData = null;
        $ret = $request->query('ret');
        $wz = $request->query('wz');

        if ($ret) {
            $request->session()->put(self::KEY_RETURN_URL, $ret);

            $token = explode('/', $ret);
            $refId = $token[5];

            /*if ($token[4] == 'odf') {
                $refData = TODO
            } elseif ($token[4] == 'odc') {
                $refData = TODO
            }*/
        } elseif ($wz) {
            $workzoneData = WorkzoneCached::getById($wz)[0];
            if ($workzoneData) {
                $refData = (object)[
                    'workzone_id' => $workzoneData->id,
                    'workzone_label' => $workzoneData->label
                ];
            }
        }

        if ($refData) {
            $feederData = (object)[
                'workzone_id' => $refData->workzone_id,
                'workzone_label' => $refData->workzone_label
            ];
        }

        $canEdit = true;
        $capacities = FeederCached::CAPACITIES;

        return view(
            'mcore.feeder.form',
            compact('workzoneTree', 'feederData', 'canEdit', 'capacities')
        );
    }

    public function create(SubmitFeeder $request)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            $id = FeederCached::create(
                $user->id,
                $request->workzone_id,
                $request->label,
                $request->capacity,
                $request->tubecount
            );
            $link = '<a href="/mcore/feeder/'.$id.'">'.$request->label.'</a>';

            $session = $request->session();
            $response = $session->has(self::KEY_RETURN_URL)
                ? redirect($session->pull(self::KEY_RETURN_URL))
                : back()
            ;

            return $response->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> menambah data Kabel Feeder '.$link
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function show($id)
    {
        [$feederData, $feed_mtime] = FeederCached::getById($id);
        if (!$feed_mtime) {
            abort(404);
        }

        $currentUser = SessionHelper::getCurrentUser();
        [$workzoneTree, $wz_mtime] = WorkzoneCached::getByPath($currentUser->workzone_path);

        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.feeder', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $feederData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        [$linkList, $links_mtime] = FeederLinkCached::getByFeeder($id);
        $coreList = FeederCached::linksToCores($feederData->capacity, $linkList->all());

        $capacities = FeederCached::CAPACITIES;

        return view(
            'mcore.feeder.form',
            compact('workzoneTree', 'feederData', 'canEdit', 'capacities', 'coreList')
        );
    }

    public function update(SubmitFeeder $request, $id)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            FeederCached::update(
                $user->id,
                $id,
                $request->workzone_id,
                $request->label,
                $request->capacity
            );

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> merubah data Kabel Feeder'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public static function showRoute($id)
    {
        [$entityData, $feed_mtime] = FeederCached::getById($id);
        if (!$feed_mtime) {
            abort(404);
        }

        $entityData->type = 'feeder';
        $entityData->shortLabel = 'FEED';

        $routeData = FeederCached::getRoutePath($id);
        if ($routeData) {
            $routeData = Mcore::refreshRouteDataPoint($routeData);
        } else {
            $routeData = [];
        }

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.feeder', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $entityData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        return view('mcore.route.form', compact('entityData', 'canEdit', 'routeData'));
    }

    public static function submitRoute(Request $request, $id)
    {
        [$feederData, $feed_mtime] = FeederCached::getById($id);
        if (!$feed_mtime) {
            abort(404);
        }

        try {
            $user = SessionHelper::getCurrentUser();
            FeederCached::saveRoutePath($user->id, $feederData->id, $request->route_path);

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Sukses</strong> merubah data Jalur Kabel Feeder'
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }
}
