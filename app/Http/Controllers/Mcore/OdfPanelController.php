<?php

namespace App\Http\Controllers\Mcore;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Http\Requests\Mcore\SubmitOdfPanel;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Mcore\StoCached;
use App\Service\Mcore\StoRoomCached;
use App\Service\Mcore\OdfCached;
use App\Service\Mcore\OdfPanelCached;
use App\Service\Mcore\Link\FeederLinkCached;

class OdfPanelController extends Controller
{
    public function rearLink($stoId, $roomId, $odfId, $panelId, $port)
    {
        $url = "/mcore/sto/$stoId/room/$roomId/odf/$odfId/panel/$panelId/port/$port/";
        $feederLink = FeederLinkCached::getByOtbPort($panelId, $port)[0];

        $url .= $feederLink ? 'odc' : 'olt';
        return redirect($url);
    }

    public function createForm($stoId, $roomId, $odfId)
    {
        [$stoData, $sto_mtime] = StoCached::getById($stoId);
        [$roomData, $room_mtime] = StoRoomCached::getById($roomId);
        [$odfData, $odf_mtime] = OdfCached::getById($odfId);

        $panelData = null;
        $canEdit = true;

        return view('mcore.odfpanel.form', compact('stoData', 'roomData', 'odfData', 'panelData', 'canEdit'));
    }

    public function create(SubmitOdfPanel $request, $stoId, $roomId, $odfId)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            $id = OdfPanelCached::create(
                $user->id,
                $odfId,
                $request->label,
                $request->slotcount,
                $request->slotportcount,
                $request->slotvertical
            );
            $link = "<a href='/mcore/sto/$stoId/room/$roomId/odf/$odfId/panel/$id'>{$request->label}</a>";

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Berhasil</strong> menambah data Panel ' . $link
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> menambah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function updateForm($stoId, $roomId, $odfId, $panelId)
    {
        [$stoData, $sto_mtime] = StoCached::getById($stoId);
        [$roomData, $room_mtime] = StoRoomCached::getById($roomId);
        [$odfData, $odf_mtime] = OdfCached::getById($odfId);
        [$panelData, $panel_mtime] = OdfPanelCached::getById($panelId);
        [$linkList, $link_mtime] = OdfPanelCached::getLinks($panelId);
        $portList = OdfPanelCached::parsePanelPort($panelData, $linkList->all());

        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = SessionHelper::currentUserHasPermission('mcore.sto.odf.panel', Authorization::WRITE);
        $isWithinZone = Authorization::isWithinZone($currentUser->workzone_path, $stoData->workzone_path);
        $canEdit = $hasWriteAccess && $isWithinZone;

        $groupedPort = [];
        $group = [];
        $i = 1;
        foreach ($portList as $port) {
            $group[] = $port;

            if (++$i > $panelData->slotportcount) {
                $groupedPort[] = $group;
                $group = [];
                $i = 1;
            }
        }

        return view(
            'mcore.odfpanel.form',
            compact('stoData', 'roomData', 'odfData', 'panelData', 'groupedPort', 'canEdit')
        );
    }

    public function update(SubmitOdfPanel $request, $stoId, $roomId, $odfId, $panelId)
    {
        try {
            $user = SessionHelper::getCurrentUser();
            OdfPanelCached::update(
                $user->id,
                $panelId,
                $request->label,
                $request->slotcount,
                $request->slotportcount,
                $request->slotvertical
            );

            return back()->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Berhasil</strong> merubah data '
                ]
            ]);
        } catch (\Throwable $e) {
            return back()->withInput()->with('alerts', [
                [
                    'type' => 'danger',
                    'text' => '<strong>GAGAL</strong> merubah data<br>'.$e->getMessage()
                ]
            ]);
        }
    }

    public function listPortByPanelAsSelect2($panelId)
    {
        [$panelData, $panel_mtime] = OdfPanelCached::getById($panelId);
        [$linkList, $link_mtime] = OdfPanelCached::getLinks($panelId);
        $portList = OdfPanelCached::parsePanelPort($panelData, $linkList->all());

        $result = [];
        $view = View::make('mcore.odfpanel.port');

        foreach ($portList as $portNum => $portData) {
            $result[] = [
                'id' => $portNum.':rear',
                'text' => $view->with(compact('portData'))->render(),
                'disabled' => $portData->rear ? true : false
            ];
        }

        return $result;
    }
}
