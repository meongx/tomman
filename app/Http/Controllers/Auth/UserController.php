<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateLocalUser;
use App\Http\Requests\UpdateUser;
use App\Service\Auth\Permission;
use App\Service\Auth\WorkzoneCached;
use App\Service\Auth\User;
use App\Service\Auth\Authorization;
use App\Service\SessionHelper;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $q = $request->input('q');
        if ($q) {
            return $this->searchUser($q);
        } else {
            return $this->listUser();
        }
    }

    public function createForm()
    {
        $currentUser = SessionHelper::getCurrentUser();
        if ($currentUser->workzone_id != '1') {
            abort(403);
        }

        $permissionList = Permission::all();
        [$workzoneTree, $workzoneLastModified] = WorkzoneCached::getByPath($currentUser->workzone_path);
        $timezoneList = User::AVAILABLE_TIMEZONE;

        return view('auth.user.create', compact('permissionList', 'workzoneTree', 'timezoneList'));
    }

    public function create(CreateLocalUser $request)
    {
        $input = $request->only([
            'login',
            'nama',
            'timezone',
            'password',
            'workzone_id',
            'permission_id',
            'permission'
        ]);
        $input['type'] = User::TYPE_LOCALUSER;

        $id = User::create($input);
        if (!$id) {
            abort(500);
        }

        return back()->with([
            'alerts' => [
                [
                    'type' => 'success',
                    'text' => "
                        <strong>Berhasil</strong> membuat user untuk 
                        <a href='/auth/user/$id'>[{$input['login']}] {$input['nama']}</a>
                    "
                ]
            ]
        ]);
    }

    public function updateForm($id)
    {
        $data = User::getById($id);
        if (!$data) {
            abort(404);
        }

        $currentUser = SessionHelper::getCurrentUser();
        $canUpdate = Authorization::hasPermission(
            'auth.user',
            Authorization::WRITE,
            $currentUser->permission
        );

        $permissionList = Permission::all();
        $timezoneList = User::AVAILABLE_TIMEZONE;
        [$workzoneTree, $workzoneLastModified] = WorkzoneCached::getByPath($currentUser->workzone_path);

        return view(
            'auth.user.update',
            compact('data', 'permissionList', 'workzoneTree', 'canUpdate', 'timezoneList')
        );
    }

    public function update(UpdateUser $request, $id)
    {
        $data = User::getById($id);
        if (!$data) {
            abort(404);
        }

        $accepts = ['timezone', 'workzone_id', 'permission_id'];

        $input = $request->only($accepts);
        if ($data->type == User::TYPE_NEWUSER) {
            $input['type'] = User::TYPE_UNKNOWN;
        }

        $affected = User::update($id, $input);
        if ($affected != 1) {
            abort($affected > 1 ? 500 : 404);
        }

        return redirect('/auth/user')->with('alerts', [
            [
                'type' => 'success',
                'text' => '<strong>Sukses</strong> melakukan update user'
            ]
        ]);
    }

    private function listUser()
    {
        $currentUser = SessionHelper::getCurrentUser();
        $hasWriteAccess = Authorization::hasPermission(
            'auth.user',
            Authorization::WRITE,
            $currentUser->permission
        );
        $canCreateNew = $hasWriteAccess && ($currentUser->workzone_id == '1');

        if ($hasWriteAccess) {
            $newUsers = User::allNewlyRegistered();
        } else {
            $newUsers = false;
        }

        $listUsers = User::paginateByWorkzonePath($currentUser->workzone_path);

        return view('auth.user.list', compact('newUsers', 'listUsers', 'canCreateNew'));
    }

    private function searchUser($searchString)
    {
        $q = $searchString;
        $currentUser = SessionHelper::getCurrentUser();

        $list = User::paginateSearch($searchString, $currentUser->workzone_path);
        $list->appends(compact('q'));

        return view('auth.user.search', compact('list', 'q'));
    }
}
