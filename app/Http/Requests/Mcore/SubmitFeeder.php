<?php

namespace App\Http\Requests\Mcore;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Mcore\Feeder;

class SubmitFeeder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return SessionHelper::currentUserHasPermission('mcore.feeder', Authorization::WRITE);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = 'required';

        return [
            'label' => $required,
            'workzone_id' => $required.'|within_user_workzone',
            'capacity' => $required.'|'.Rule::in(Feeder::CAPACITIES)
        ];
    }
}
