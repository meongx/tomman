<?php

namespace App\Http\Requests\Mcore;

use App\Http\Requests\FormRequestWithCoordinate;
use App\Service\Auth\Authorization;
use App\Service\Mcore\Odp;
use App\Service\SessionHelper;
use Illuminate\Validation\Rule;

class SubmitOdp extends FormRequestWithCoordinate
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return SessionHelper::currentUserHasPermission('mcore.odp', Authorization::WRITE);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = 'required';

        return [
            'label' => $required,
            'workzone_id' => $required.'|within_user_workzone',
            'type' => $required.'|'.Rule::in(array_keys(Odp::TYPES)),
            'status' => $required.'|'.Rule::in(array_keys(Odp::STATUSES)),
            'capacity' => $required.'|'.Rule::in(Odp::CAPACITIES)
        ];
    }
}
