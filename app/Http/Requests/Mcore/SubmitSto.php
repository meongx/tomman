<?php

namespace App\Http\Requests\Mcore;

use App\Http\Requests\FormRequestWithCoordinate;
use App\Service\Auth\Authorization;
use App\Service\SessionHelper;

class SubmitSto extends FormRequestWithCoordinate
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return SessionHelper::currentUserHasPermission('mcore.sto', Authorization::WRITE);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => 'required',
            'workzone_id' => 'required|within_user_workzone'
        ];
    }
}
