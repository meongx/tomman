<?php

namespace App\Http\Requests\Mcore;

use App\Http\Requests\FormRequestWithCoordinate;
use Illuminate\Validation\Rule;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Mcore\Odc;

class SubmitOdc extends FormRequestWithCoordinate
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return SessionHelper::currentUserHasPermission('mcore.odc', Authorization::WRITE);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = 'required';

        return [
            'label' => $required,
            'workzone_id' => $required.'|within_user_workzone',
            'panelcount' => $required.'|numeric',
            'portperpanel' => $required.'|'.Rule::in(Odc::PORT_COUNTS),
            'type' => $required.'|'.Rule::in(array_keys(Odc::TYPES))
        ];
    }
}
