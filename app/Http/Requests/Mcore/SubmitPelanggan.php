<?php

namespace App\Http\Requests\Mcore;

use App\Http\Requests\FormRequestWithCoordinate;
use App\Service\Auth\Authorization;
use App\Service\Mcore\Pelanggan;
use App\Service\SessionHelper;
use Illuminate\Validation\Rule;

class SubmitPelanggan extends FormRequestWithCoordinate
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return SessionHelper::currentUserHasPermission('mcore.pelanggan', Authorization::WRITE);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = 'required';

        return [
            'kode' => $required,
            'workzone_id' => $required.'|within_user_workzone',
            'label' => $required,
            'type' => $required.'|'.Rule::in(array_keys(Pelanggan::TYPES)),
        ];
    }
}
