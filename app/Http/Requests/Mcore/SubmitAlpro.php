<?php

namespace App\Http\Requests\Mcore;

use Illuminate\Validation\Rule;
use App\Http\Requests\FormRequestWithCoordinate;
use App\Service\SessionHelper;
use App\Service\Auth\Authorization;
use App\Service\Mcore\Alpro;

class SubmitAlpro extends FormRequestWithCoordinate
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return SessionHelper::currentUserHasPermission('mcore.alpro', Authorization::WRITE);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = 'required';

        return [
            'workzone_id' => $required.'|within_user_workzone',
            'type' => $required.'|'.Rule::in(array_keys(Alpro::TYPES))
        ];
    }
}
