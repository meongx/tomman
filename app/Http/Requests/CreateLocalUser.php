<?php

namespace App\Http\Requests;

use App\Service\Auth\Authorization;
use App\Service\SessionHelper;
use Illuminate\Foundation\Http\FormRequest;

class CreateLocalUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $currentUser = SessionHelper::getCurrentUser();
        $canWrite = Authorization::hasPermission(
            'auth.user',
            Authorization::WRITE,
            $currentUser->permission
        );
        $notBoundToWorkzone = $currentUser->workzone_id == 1;

        return ($canWrite && $notBoundToWorkzone);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = 'required';

        return [
            'login' => $required,
            'password' => $required,
            'nama' => $required,
            'timezone' => $required,
            'workzone_id' => $required,
            'permission_id' => $required,
            'permission' => 'valid_permission'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'workzone_id.required' => 'Silahkan pilih Workzone',
            'permission_id.required' => 'Silahkan pilih Permission'
        ];
    }
}
