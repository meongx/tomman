<?php

namespace App\Service\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Service\Auth\Authorization as Auth;

class User
{
    const TYPE_UNKNOWN = -1;
    const TYPE_NEWUSER = 0;
    const TYPE_LOCALUSER = 1;

    const AVAILABLE_TIMEZONE = [
        0 => ['WIB', 'Asia/Jakarta'],
        1 => ['WITA', 'Asia/Makassar'],
        2 => ['WIT', 'Asia/Jayapura']
    ];

    private static function table()
    {
        return DB::table('auth.user AS user');
    }

    private static function db()
    {
        return self::table()
            ->leftJoin('auth.workzone AS workzone', 'user.workzone_id', '=', 'workzone.id')
            ->leftJoin('auth.permission AS perm', 'user.permission_id', '=', 'perm.id')
            ->select(
                'user.id',
                'user.is_login_enabled',
                'user.type',
                'user.timezone',
                'user.login',
                'user.nama',
                'user.hash',
                'user.remember_token',
                'user.sso_cookie',
                //
                'user.workzone_id',
                'workzone.label AS workzone_label',
                'workzone.path AS workzone_path',
                //
                'user.permission_id',
                'perm.title AS permission_title',
                'perm.permission AS permission_group',
                'user.permission AS permission_override'
            );
    }

    public static function allNewlyRegistered()
    {
        return self::db()->where('type', self::TYPE_NEWUSER)->get();
    }

    public static function paginateByWorkzonePath($path, $limit = 25)
    {
        return self::db()
            ->where([
                ['type', '<>', self::TYPE_NEWUSER],
                ['path', '<@', $path]
            ])
            ->paginate($limit);
    }

    public static function paginateSearch($searchString, $workzonePath, $limit = 25)
    {
        $searchString = '%'.str_replace(' ', '%', $searchString).'%';
        $query = self::db()
            ->where(function ($q) use ($searchString) {
                $q->where('login', 'ILIKE', $searchString);
                $q->orWhere('user.nama', 'ILIKE', $searchString);
            });

        if ($workzonePath) {
            $query->where(function ($q) use ($workzonePath) {
                $q->where('type', self::TYPE_NEWUSER);
                $q->orWhere('path', '<@', $workzonePath);
            });
        }

        return $query->paginate($limit);
    }

    public static function getByLocalCredential($user, $pass)
    {
        $result = self::db()->where([
            'login' => $user,
            'type'  => self::TYPE_LOCALUSER
        ])->first();

        if (!$result || !Hash::check($pass, $result->hash)) {
            return false;
        }

        return self::normalizeAttribute($result);
    }

    public static function getByRememberToken($token)
    {
        $result = self::db()->where('remember_token', $token)->first();

        if ($result) {
            return self::normalizeAttribute($result);
        } else {
            return false;
        }
    }

    public static function getByLogin($login)
    {
        $result = self::db()->where('login', $login)->first();

        if ($result) {
            return self::normalizeAttribute($result);
        } else {
            return false;
        }
    }

    public static function getById($id)
    {
        $result = self::db()->where('user.id', $id)->first();

        if ($result) {
            return self::normalizeAttribute($result);
        } else {
            return false;
        }
    }

    public static function create(array $data)
    {
        if (isset($data['password'])) {
            $data['hash'] = Hash::make($data['password']);
            unset($data['password']);
        }

        return self::table()->insertGetId($data);
    }

    public static function update($id, array $data)
    {
        return self::table()->where('user.id', $id)->update($data);
    }

    private static function normalizeAttribute($user)
    {
        $user = self::deserializePermission($user);
        $user->phpTimezone = self::deserializeTimezone($user->timezone);

        return $user;
    }

    private static function deserializeTimezone($timezone)
    {
        return new \DateTimeZone(self::AVAILABLE_TIMEZONE[$timezone][1]);
    }

    private static function deserializePermission($user)
    {
        $perm = $user->permission_group . "\n" . $user->permission_override;
        $user->permission = Auth::deserializePermission($perm);

        return $user;
    }
}
