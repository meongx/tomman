<?php

namespace App\Service\Auth;

use Illuminate\Support\Facades\DB;

class Permission
{
    private static function db()
    {
        return DB::table('auth.permission');
    }

    public static function all()
    {
        return self::db()->orderBy('title')->get();
    }

    public static function paginate($limit = 25)
    {
        return self::db()->orderBy('title')->paginate($limit);
    }

    public static function paginateWithSearch($query, $limit = 25)
    {
        return self::db()
            ->where('title', 'ILIKE', "%{$query}%")
            ->paginate($limit);
    }

    public static function create($title, $permission)
    {
        return self::db()->insertGetId(compact('title', 'permission'));
    }

    public static function update($id, $title, $permission)
    {
        return self::db()
            ->where('id', $id)
            ->update(compact('title', 'permission'));
    }
}
