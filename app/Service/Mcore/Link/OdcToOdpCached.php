<?php

namespace App\Service\Mcore\Link;

use App\Service\Cache;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\DistribusiCached;
use App\Service\Mcore\OdcCached;
use App\Service\Mcore\OdpCached;

class OdcToOdpCached
{
    const PREFIX = 'Mcore.Link.OdcOdp:';
    const KEY_BY_ODC_PANEL_PORT = self::PREFIX.'Odc=%s;Panel=%s;Port=%s';
    const KEY_BY_DIST = self::PREFIX.'Disttribusi=';
    const KEY_BY_ODP = self::PREFIX.'Odp=';

    private static function flush($odcId, $distribusiId, $odpId)
    {
        $tags = [
            OdcCached::tagById($odcId),
            DistribusiCached::tagById($distribusiId),
            OdpCached::tagById($odpId)
        ];

        // flush ODP list due to source link
        [$odpData, $odp_mtime] = Cache::get(OdpCached::keyById($odpId));
        if ($odp_mtime !== null) {
            $tags = array_merge(
                $tags,
                WorkzoneCached::tagsByWorkzonePath(
                    $odpData->workzone_path,
                    [OdpCached::class, 'tagByWorkzoneId']
                )
            );
        }

        Cache::flushTags($tags);
    }

    /**
     * @param int $userId for history/audit
     * @param int $odcId
     * @param string $odcVal
     * @param int $distribusiId
     * @param string $distribusiVal
     * @param int $odpId
     * @throws \Throwable when database transaction failed
     */
    public static function plug(
        int $userId,
        int $odcId,
        string $odcVal,
        int $distribusiId,
        string $distribusiVal,
        int $odpId
    ) {
        self::flush($odcId, $distribusiId, $odpId);
        OdcToOdp::plug($userId, $odcId, $odcVal, $distribusiId, $distribusiVal, $odpId);
    }

    /**
     * @param int $userId for history/audit
     * @param int $odcId
     * @param string $odcVal
     * @param int $distribusiId
     * @param string $distribusiVal
     * @param int $odpId
     * @throws \Throwable when database transaction failed
     */
    public static function unplug(
        int $userId,
        int $odcId,
        string $odcVal,
        int $distribusiId,
        string $distribusiVal,
        int $odpId
    ) {
        self::flush($odcId, $distribusiId, $odpId);
        OdcToOdp::unplug($userId, $odcId, $odcVal, $distribusiId, $distribusiVal, $odpId);
    }

    public static function getByOdcPanelPort($id, $panel, $port)
    {
        return Cache::store(
            sprintf(self::KEY_BY_ODC_PANEL_PORT, $id, $panel, $port),
            function () use ($id, $panel, $port) {
                return OdcToOdp::getByOdcPanelPort($id, $panel, $port);
            },
            function () use ($id) {
                return [OdcCached::tagById($id)];
            }
        );
    }

    public static function getByDistribusi($id)
    {
        return Cache::store(
            self::KEY_BY_DIST.$id,
            function () use ($id) {
                return OdcToOdp::getByDistribusi($id);
            },
            function () use ($id) {
                return [DistribusiCached::tagById($id)];
            }
        );
    }

    public static function getByOdp($id)
    {
        return Cache::store(
            self::KEY_BY_ODP.$id,
            function () use ($id) {
                return OdcToOdp::getByOdp($id);
            },
            function () use ($id) {
                return [OdpCached::tagById($id)];
            }
        );
    }
}
