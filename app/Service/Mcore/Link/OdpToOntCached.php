<?php

namespace App\Service\Mcore\Link;

use App\Service\Cache;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\OdpCached;
use App\Service\Mcore\PelangganCached;

class OdpToOntCached
{
    public const SRC_TYPE = OdpToOnt::SRC_TYPE;
    public const DST_TYPE = OdpToOnt::DST_TYPE;

    const PREFIX = 'Mcore.Link.OdpToOnt:';
    const KEY_BY_ODP = self::PREFIX.'Odp=';
    const KEY_BY_PELANGGAN = self::PREFIX.'Pelanggan=';
    const KEY_PELANGGAN_BY_WORKZONE = self::PREFIX.'Pelanggan;WorkzonePath=%s;Page=%s;Search=%s;Limit=%s';

    /**
     * @param int $user_id for history/audit
     * @param int $odpId
     * @param int $odpPort
     * @param string $mediumLabel
     * @param int $pelangganId
     * @throws \Throwable when database transaction failed
     */
    public static function save(int $user_id, int $odpId, int $odpPort, string $mediumLabel, int $pelangganId)
    {
        OdpToOnt::save($user_id, $odpId, $odpPort, $mediumLabel, $pelangganId);

        $tags = [OdpCached::tagById($odpId), PelangganCached::tagById($pelangganId)];

        // need to flush Pelanggan List due to paginatePelangganByWorkzonePath()
        $pelangganData = PelangganCached::getById($pelangganId)[0];
        $tags = array_merge(
            $tags,
            WorkzoneCached::tagsByWorkzoneId($pelangganData->workzone_id, [PelangganCached::class, 'tagByWorkzoneId'])
        );

        Cache::flushTags($tags);
    }

    /**
     * @param int $user_id for history/audit
     * @param int $odpId
     * @param int $odpPort
     * @param int $pelangganId
     * @throws \Throwable when database transaction failed
     */
    public static function remove(int $user_id, int $odpId, int $odpPort, int $pelangganId)
    {
        $tags = [OdpCached::tagById($odpId), PelangganCached::tagById($pelangganId)];

        // need to flush Pelanggan List due to self::paginatePelangganByWorkzonePath()
        $pelangganData = PelangganCached::getById($pelangganId)[0];
        $tags = array_merge(
            $tags,
            WorkzoneCached::tagsByWorkzoneId($pelangganData->workzone_id, [PelangganCached::class, 'tagByWorkzoneId'])
        );

        Cache::flushTags($tags);

        OdpToOnt::remove($user_id, $odpId, $odpPort, $pelangganId);
    }

    public static function getByOdp($odpId, $odpPort = false)
    {
        $key = self::KEY_BY_ODP.$odpId;
        if ($odpPort) {
            $key .= ";Port=$odpPort";
        }

        $dataSource = function () use ($odpId, $odpPort) {
            return OdpToOnt::getByOdp($odpId, $odpPort);
        };

        $tagGenerator = function () use ($odpId) {
            return [OdpCached::tagById($odpId)];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    public static function getByPelanggan($pelangganId)
    {
        return Cache::store(
            self::KEY_BY_PELANGGAN.$pelangganId,
            function () use ($pelangganId) {
                return OdpToOnt::getByPelanggan($pelangganId);
            },
            function () use ($pelangganId) {
                return [PelangganCached::tagById($pelangganId)];
            }
        );
    }

    public static function paginatePelangganByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $ttl = isset($search) ? 60 * 60 : 0;
        $key = sprintf(self::KEY_PELANGGAN_BY_WORKZONE, $path, $page, $search, $limit);

        $dataSource = function () use ($path, $page, $search, $limit) {
            return OdpToOnt::paginatePelangganByWorkzonePath($path, $page, $search, $limit);
        };

        $tagGenerator = function () use ($path) {
            return WorkzoneCached::tagsByWorkzonePath($path, [PelangganCached::class, 'tagByWorkzoneId']);
        };

        return Cache::store($key, $dataSource, $tagGenerator, $ttl);
    }
}
