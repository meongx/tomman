<?php

namespace App\Service\Mcore\Link;

use Illuminate\Support\Facades\DB;
use App\Service\Mcore\Helper as Mcore;
use App\Service\Mcore\OdfPanelCached;
use App\Service\Mcore\FeederCached;
use App\Service\Mcore\OdcCached;

class FeederLink
{
    private static function table()
    {
        return DB::table('mcore.link');
    }

    private static function db()
    {
        $q = self::table();

        $q
            ->join('mcore.odf_panel', function ($join) {
                $join->on('link.src_type', DB::raw("'".Mcore::TYPE_ODF_PANEL."'"))
                     ->on('link.src_id', 'odf_panel.id');
            })

            ->join('mcore.feeder', function ($join) {
                $join->on('link.med_type', DB::raw("'".Mcore::TYPE_FEEDER."'"))
                     ->on('link.med_id', 'feeder.id');
            })
            ->join('auth.workzone AS wz_feeder', 'feeder.workzone_id', '=', 'wz_feeder.id')

            ->join('mcore.odc', function ($join) {
                $join->on('link.dst_type', DB::raw("'".Mcore::TYPE_ODC."'"))
                     ->on('link.dst_id', 'odc.id');
            })
            ->join('auth.workzone AS wz_odc', 'odc.workzone_id', '=', 'wz_odc.id')

            ->select(
                'link.*',
                //
                'odf_panel.id AS odf_panel_id',
                'odf_panel.label AS odf_panel_label',
                //
                'feeder.id AS feeder_id',
                'feeder.label AS feeder_label',
                'feeder.workzone_id AS feeder_workzone_id',
                'wz_feeder.label AS feeder_workzone_label',
                //
                'odc.id AS odc_id',
                'odc.label AS odc_label',
                'odc.workzone_id AS odc_workzone_id',
                'wz_odc.label AS odc_workzone_label'
            )
        ;

        Mcore::odfPanelJoins($q);

        return $q;
    }

    public static function getByOdcPanelPort($id, $panel, $port)
    {
        return self::db()
            ->where('dst_type', Mcore::TYPE_ODC)
            ->where('dst_id', $id)
            ->where('dst_val', "$panel:$port:rear")
            ->first()
        ;
    }

    public static function getByOtbPort($id, $port)
    {
        return self::db()
            ->where('src_type', Mcore::TYPE_OTB)
            ->where('src_id', $id)
            ->where('src_val', "$port:rear")
            ->first()
        ;
    }

    public static function getByFeeder($id)
    {
        return self::db()
            ->where('med_type', 'feeder')
            ->where('med_id', $id)
            ->get()
        ;
    }

    /**
     * @param $userId
     * @param $otbId
     * @param $otbVal
     * @param $feederId
     * @param $feederVal
     * @param $odcId
     * @param $odcVal
     * @throws \Throwable
     */
    public static function plug($userId, $otbId, $otbVal, $feederId, $feederVal, $odcId, $odcVal)
    {
        $data = [
            'src_type' => 'odf_panel',
            'src_id' => $otbId,
            'src_val' => $otbVal,
            'med_type' => 'feeder',
            'med_id' => $feederId,
            'med_val' => $feederVal,
            'dst_type' => 'odc',
            'dst_id' => $odcId,
            'dst_val' => $odcVal
        ];

        DB::transaction(function () use ($userId, $data) {
            self::table()->insert($data);

            $operation = 'plug';
            OdfPanelCached::insertHistory($userId, $data['src_id'], $operation, $data);
            FeederCached::insertHistory($userId, $data['med_id'], $operation, $data);
            OdcCached::insertHistory($userId, $data['dst_id'], $operation, $data);
        });
    }

    /**
     * @param $userId
     * @param $otbId
     * @param $otbVal
     * @param $feederId
     * @param $feederVal
     * @param $odcId
     * @param $odcVal
     * @throws \Throwable
     */
    public static function unplug($userId, $otbId, $otbVal, $feederId, $feederVal, $odcId, $odcVal)
    {
        $data = [
            'src_type' => 'odf_panel',
            'src_id' => $otbId,
            'src_val' => $otbVal,
            'med_type' => 'feeder',
            'med_id' => $feederId,
            'med_val' => $feederVal,
            'dst_type' => 'odc',
            'dst_id' => $odcId,
            'dst_val' => $odcVal
        ];

        DB::transaction(function () use ($userId, $data) {
            self::table()->where($data)->delete();

            $operation = 'unplug';
            OdfPanelCached::insertHistory($userId, $data['src_id'], $operation, $data);
            FeederCached::insertHistory($userId, $data['med_id'], $operation, $data);
            OdcCached::insertHistory($userId, $data['dst_id'], $operation, $data);
        });
    }
}
