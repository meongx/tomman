<?php

namespace App\Service\Mcore\Link;

use Illuminate\Support\Facades\DB;
use App\Service\Mcore\Odc;

class OdcSplitter
{
    public const TYPE_ODC = 'odc';
    public const TYPE_SPLIITER = 'odc_splitter';

    public const ODC_PORT_POS = 'front';

    private static function table()
    {
        return DB::table('mcore.link AS link');
    }

    /**
     * @param $userId
     * @param $odcId
     * @param $odcPanel
     * @param $odcPort
     * @param $splitterId
     * @param $splitterPort
     * @throws \Throwable
     */
    public static function plug($userId, $odcId, $odcPanel, $odcPort, $splitterId, $splitterPort)
    {
        $odcPanelPort = "$odcPanel:$odcPort:".self::ODC_PORT_POS;

        if ($splitterPort <= 0) {
            $data = [
                'src_type' => self::TYPE_ODC,
                'src_id' => $odcId,
                'src_val' => $odcPanelPort,

                'dst_type' => self::TYPE_SPLIITER,
                'dst_id' => $splitterId,
                'dst_val' => $splitterPort
            ];
        } else {
            $data = [
                'src_type' => self::TYPE_SPLIITER,
                'src_id' => $splitterId,
                'src_val' => $splitterPort,

                'dst_type' => self::TYPE_ODC,
                'dst_id' => $odcId,
                'dst_val' => $odcPanelPort
            ];
        }

        DB::transaction(function () use ($userId, $odcId, $data) {
            self::table()->insert($data);

            Odc::insertHistory($userId, $odcId, 'link.plug', $data);
        });
    }

    /**
     * @param $userId
     * @param $odcId
     * @param $odcPanel
     * @param $odcPort
     * @throws \Throwable
     */
    public static function unplugByOdc($userId, $odcId, $odcPanel, $odcPort)
    {
        $odcPanelPort = "$odcPanel:$odcPort:";

        DB::transaction(function () use ($userId, $odcId, $odcPanelPort) {
            self::table()
                ->where(function ($query) use ($odcId, $odcPanelPort) {
                    $query
                        ->where('src_type', self::TYPE_ODC)
                        ->where('src_id', $odcId)
                        ->where('src_val', $odcPanelPort.self::ODC_PORT_POS);
                })
                ->orWhere(function ($query) use ($odcId, $odcPanelPort) {
                    $query
                        ->where('dst_type', self::TYPE_ODC)
                        ->where('dst_id', $odcId)
                        ->where('dst_val', $odcPanelPort.self::ODC_PORT_POS);
                })
                ->delete();

            Odc::insertHistory($userId, $odcId, 'link.unplug', compact('odcPanelPort'));
        });
    }
}
