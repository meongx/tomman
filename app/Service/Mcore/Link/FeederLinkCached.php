<?php

namespace App\Service\Mcore\Link;

use App\Service\Cache;
use App\Service\Auth\WorkzoneCached;
use App\Service\Mcore\OdfPanelCached;
use App\Service\Mcore\FeederCached;
use App\Service\Mcore\OdcCached;

class FeederLinkCached
{
    const PREFIX = 'Mcore.Link.OtbOdc:';
    const KEY_BY_ODC_PANEL_PORT = self::PREFIX.'Odc=%s;Panel=%s;Port=%s';
    const KEY_BY_FEED = self::PREFIX.'FEEDER=';
    const KEY_BY_OTB_PORT = self::PREFIX.'Otb=%s;Port=%s';

    private static function flush($otbId, $feederId, $odcId)
    {
        $tags = [
            OdfPanelCached::tagById($otbId),
            FeederCached::tagById($feederId),
            OdcCached::tagById($odcId)
        ];

        // flush ODC list due to capacity and usage
        [$odcData, $odc_mtime] = Cache::get(OdcCached::keyById($odcId));
        if ($odc_mtime !== null) {
            $tags = array_merge(
                $tags,
                WorkzoneCached::tagsByWorkzonePath(
                    $odcData->workzone_path,
                    [OdcCached::class, 'tagByWorkzoneId']
                )
            );
        }

        Cache::flushTags($tags);
    }
    public static function getByOdcPanelPort($id, $panel, $port)
    {
        return Cache::store(
            sprintf(self::KEY_BY_ODC_PANEL_PORT, $id, $panel, $port),
            function () use ($id, $panel, $port) {
                return FeederLink::getByOdcPanelPort($id, $panel, $port);
            },
            function () use ($id) {
                return [OdcCached::tagById($id)];
            }
        );
    }

    public static function getByOtbPort($id, $port)
    {
        return Cache::store(
            sprintf(self::KEY_BY_OTB_PORT, $id, $port),
            function () use ($id, $port) {
                return FeederLink::getByOtbPort($id, $port);
            },
            function () use ($id) {
                return [OdfPanelCached::tagById($id)];
            }
        );
    }

    public static function getByFeeder($id)
    {
        return Cache::store(
            self::KEY_BY_FEED.$id,
            function () use ($id) {
                return FeederLink::getByFeeder($id);
            },
            function () use ($id) {
                return [FeederCached::tagById($id)];
            }
        );
    }

    /**
     * @param $userId
     * @param $otbId
     * @param $otbVal
     * @param $feederId
     * @param $feederVal
     * @param $odcId
     * @param $odcVal
     * @throws \Throwable
     */
    public static function plug($userId, $otbId, $otbVal, $feederId, $feederVal, $odcId, $odcVal)
    {
        self::flush($otbId, $feederId, $odcId);
        FeederLink::plug($userId, $otbId, $otbVal, $feederId, $feederVal, $odcId, $odcVal);
    }

    /**
     * @param $userId
     * @param $otbId
     * @param $otbVal
     * @param $feederId
     * @param $feederVal
     * @param $odcId
     * @param $odcVal
     * @throws \Throwable
     */
    public static function unplug($userId, $otbId, $otbVal, $feederId, $feederVal, $odcId, $odcVal)
    {
        self::flush($otbId, $feederId, $odcId);
        FeederLink::unplug($userId, $otbId, $otbVal, $feederId, $feederVal, $odcId, $odcVal);
    }
}
