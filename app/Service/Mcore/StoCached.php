<?php

namespace App\Service\Mcore;

use App\Service\Cache;
use App\Service\Auth\WorkzoneCached;

class StoCached
{
    const PREFIX = 'Mcore.Sto:';
    const TAG_LISTING = self::PREFIX.'List';

    const KEY_COUNT = self::PREFIX.'Count;WorkzonePath=';
    const KEY_BY_ID = self::PREFIX.'Id=';
    const KEY_BY_WORKZONE = self::PREFIX.'WorkzonePath=%s;Page=%s;Search=%s;Limit=%s';

    public static function tagById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function tagByWorkzoneId($workzoneId)
    {
        return WorkzoneCached::tagById($workzoneId).';'.self::TAG_LISTING;
    }

    public static function keyById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function getById($id)
    {
        $key = self::keyById($id);

        $dataSource = function () use ($id) {
            return Sto::getById($id);
        };

        $tagGenerator = function () use ($id) {
            return [self::tagById($id)];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    public static function paginateByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $ttl = isset($search) ? 60 * 60 : 0;
        $key = sprintf(self::KEY_BY_WORKZONE, $path, $page, $search, $limit);

        $dataSource = function () use ($path, $page, $search, $limit) {
            return Sto::paginateByWorkzonePath($path, $page, $search, $limit);
        };

        $tagGenerator = function () use ($path) {
            return WorkzoneCached::tagsByWorkzonePath($path, [self::class, 'tagByWorkzoneId']);
        };

        return Cache::store($key, $dataSource, $tagGenerator, $ttl);
    }

    public static function countByWorkzonePath($path)
    {
        $key = self::KEY_COUNT.$path;

        $dataStore = function () use ($path) {
            return Sto::countByWorkzonePath($path);
        };

        $tagGenerator = function () use ($path) {
            $id = WorkzoneCached::idByPath($path);
            $tag = self::tagByWorkzoneId($id);

            return [$tag];
        };

        return Cache::store($key, $dataStore, $tagGenerator);
    }

    /**
     * flush: WorkzoneCached::flushTagByWorkzoneId
     *
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param string $label
     * @param float $lat
     * @param float $lng
     * @return int
     * @throws \Throwable when database transaction failed
     */
    public static function create(int $user_id, int $workzone_id, string $label, float $lat, float $lng)
    {
        $id = Sto::create($user_id, $workzone_id, $label, $lat, $lng);

        WorkzoneCached::flushTagByWorkzoneId($workzone_id, [self::class, 'tagByWorkzoneId']);

        return $id;
    }

    /**
     * flush: WorkzoneCached::tagsByWorkzoneTree, tagById
     *
     * @param int $user_Id for history/audit
     * @param int $sto_id
     * @param int $workzone_id
     * @param string $label
     * @param float $lat
     * @param float $lng
     * @throws \Throwable when database transaction failed
     */
    public static function update(int $user_Id, int $sto_id, int $workzone_id, string $label, float $lat, float $lng)
    {
        Sto::update($user_Id, $sto_id, $workzone_id, $label, $lat, $lng);

        $key = self::keyById($sto_id);
        WorkzoneCached::flushIfKeyExists(
            $key,
            $workzone_id,
            'workzone_id',
            'workzone_path',
            [self::class, 'tagByWorkzoneId'],
            [self::tagById($sto_id)]
        );
    }

    public static function insertHistory($user_id, $sto_id, $operation, array $data)
    {
        Sto::insertHistory($user_id, $sto_id, $operation, $data);

        Cache::del(self::keyById($sto_id));
    }

    /**
     * Not Cached
     *
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public static function allHistoryById($id)
    {
        return Sto::allHistoryById($id);
    }
}
