<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;
use App\Service\Auth\Workzone;

class Odp
{
    public const STATUSES = [
        0 => 'No Label',
        1 => 'No R2C',
        2 => 'Full',
        3 => 'R2C'
    ];

    public const TYPES = [
        0 => 'Pole Building (PB)',
        1 => 'Closure Aerial (CL)',
        2 => 'Pillar (PL)'
    ];

    public const CAPACITIES = [8, 12, 16];

    private static function table()
    {
        return DB::table('mcore.odp AS odp');
    }

    private static function db()
    {
        return self::table()
            ->leftJoin('auth.workzone AS workzone', 'odp.workzone_id', '=', 'workzone.id')
            ->select(
                'odp.id',
                'odp.label',
                'odp.workzone_id',
                'tenoss',
                'odp.type',
                'status',
                'capacity',
                //
                'workzone.label AS workzone_label',
                'workzone.path AS workzone_path',
                //
                DB::raw('ST_X(odp.coordinate) AS lng'),
                DB::raw('ST_Y(odp.coordinate) AS lat')
            );
    }

    private static function dbWithSourceLink()
    {
        return self::db()
            ->leftJoin('mcore.link', function ($join) {
                $join->on('link.dst_type', DB::raw("'odp'"))
                     ->on('link.dst_id', 'odp.id');
            })
            ->leftJoin('mcore.odc', function ($join) {
                $join->on('link.src_type', DB::raw("'odc'"))
                     ->on('link.dst_id', 'odc.id');
            })
            ->leftJoin('auth.workzone AS wz_odc', 'odc.workzone_id', '=', 'wz_odc.id')
            ->addSelect(
                'odc.id AS odc_id',
                'odc.label AS odc_label',
                'odc.workzone_id AS odc_workzone_id',
                'wz_odc.label AS odc_workzone_label'
            )
        ;
    }

    private static function auditTable()
    {
        return DB::table('mcore.odp_audit AS audit');
    }

    public static function countByWorkzonePath($path)
    {
        $sql = "
            SELECT
              id,
              label,
              path,
              '/mcore/odp/workzone/' || id AS url,
              CASE 
                WHEN odp.count IS NOT NULL THEN odp.count
                ELSE 0
              END AS count
              
            FROM
              auth.workzone
              
            LEFT JOIN
              (SELECT workzone_id, COUNT(workzone_id) FROM mcore.odp GROUP BY workzone_id) AS odp
              ON odp.workzone_id = id
              
            WHERE
              path <@ ?
              
            ORDER BY
              path
        ";
        $params = [$path];
        $result = DB::select($sql, $params);

        return Workzone::grow($result);
    }

    /**
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param string $label
     * @param string $tenoss
     * @param int $type
     * @param int $status
     * @param int $capacity
     * @param float $lat
     * @param float $lng
     * @return int
     * @throws \Throwable when database transaction failed
     */
    public static function create(
        int $user_id,
        int $workzone_id,
        string $label,
        string $tenoss,
        int $type,
        int $status,
        int $capacity,
        float $lat,
        float $lng
    ) {
        $id = 0;
        $data = compact('label', 'workzone_id', 'tenoss', 'type', 'status', 'capacity');

        DB::transaction(function () use (&$id, $data, $user_id, $lat, $lng) {
            $data['coordinate'] = DB::raw("ST_SetSRID(ST_MakePoint({$lng}, {$lat}), 4326)");

            $id = self::table()->insertGetId($data);

            $data['coordinate'] = compact('lat', 'lng');
            self::insertHistory($user_id, $id, 'insert', $data);
        });

        return $id;
    }

    public static function insertHistory($user_id, $odp_id, $operation, array $data)
    {
        $insertData = [
            'odp_id' => $odp_id,
            'user_id' => $user_id,
            'timestamp' => DB::raw("NOW() AT TIME ZONE 'utc'"),
            'operation' => $operation,
            'data' => json_encode($data)
        ];
        self::auditTable()->insert($insertData);
    }

    public static function getById($id)
    {
        // TODO: lastOp

        $data = self::db()->where('odp.id', $id)->first();

        return $data;
    }

    /**
     * @param int $user_id for history/audit
     * @param int $odp_id
     * @param int $workzone_id
     * @param string $label
     * @param string $tenoss
     * @param int $type
     * @param int $status
     * @param int $capacity
     * @param float $lat
     * @param float $lng
     * @throws \Throwable when database transaction failed
     */
    public static function update(
        int $user_id,
        int $odp_id,
        int $workzone_id,
        string $label,
        ?string $tenoss,
        int $type,
        int $status,
        int $capacity,
        float $lat,
        float $lng
    ) {
        $data = compact('label', 'workzone_id', 'tenoss', 'type', 'status', 'capacity');
        DB::transaction(function () use ($user_id, $odp_id, $data, $lat, $lng) {
            $data['coordinate'] = DB::raw("ST_SetSRID(ST_MakePoint({$lng}, {$lat}), 4326)");

            self::table()->where('id', $odp_id)->update($data);

            $data['coordinate'] = compact('lat', 'lng');
            self::insertHistory($user_id, $odp_id, 'update', $data);
        });
    }

    private static function generatePaginationQuery($query, $search, $page, $limit)
    {
        if ($search) {
            $clause = '(odp.label ILIKE ? OR tenoss ILIKE ?)';
            $term = '%'.str_replace(' ', '%', $search).'%';
            $param = array_fill(0, 2, "%$term%");

            $query->whereRaw($clause, $param);
        }

        return $query->paginate($limit, ['*'], 'NOTUSED', $page);
    }

    public static function paginateByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $query = self::db()->where('workzone.path', '<@', $path);

        return self::generatePaginationQuery($query, $search, $page, $limit);
    }

    public static function paginateByWorkzonePathWithSourceLink($path, $page = 1, $search = null, $limit = 25)
    {
        $query = self::dbWithSourceLink()->where('workzone.path', '<@', $path);

        return self::generatePaginationQuery($query, $search, $page, $limit);
    }

    public static function linksToPorts($capacity, array $links)
    {
        if (!count($links)) {
            return array_fill(1, $capacity, false);
        }

        $result = [];
        $link = reset($links);
        for ($i = 1; $i <= $capacity; $i++) {
            if ($link && $link->src_val == $i) {
                $result[$i] = $link;
                $link = next($links);
            } else {
                $result[$i] = false;
            }
        }

        return $result;
    }
}
