<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;

class OdcSplitter
{
    private static function table()
    {
        return DB::table('mcore.odc_splitter AS splitter');
    }

    /**
     * @param $user_id
     * @param $odc_id
     * @param $label
     * @param $odc_panel
     * @param $odc_port
     * @param $splitter_port
     * @throws \Throwable
     */
    public static function create($user_id, $odc_id, $label, $odc_panel, $odc_port, $splitter_port)
    {
        DB::transaction(function () use ($user_id, $odc_id, $label, $odc_panel, $odc_port, $splitter_port) {
            $splitterData = compact('odc_id', 'label');
            $id = self::table()->insertGetId($splitterData);

            Odc::insertHistory($user_id, $splitterData['odc_id'], 'insert.splitter', $splitterData);

            Link\OdcSplitter::plug($user_id, $odc_id, $odc_panel, $odc_port, $id, $splitter_port);
        });
    }

    public static function getByOdc($id)
    {
        $result = self::table()
            ->where('odc_id', $id)
            ->get()
            ->all();

        usort($result, function ($a, $b) {
            return strnatcmp($a->label, $b->label);
        });

        return $result;
    }

    /**
     * @param $user_id
     * @param $odc_id
     * @param $splitter_id
     * @param $label
     * @throws \Throwable
     */
    public static function update($user_id, $odc_id, $splitter_id, $label)
    {
        DB::transaction(function () use ($user_id, $odc_id, $splitter_id, $label) {
            $data = compact('label');

            self::table()->where('id', $splitter_id)->update($data);
            Odc::insertHistory($user_id, $odc_id, 'update.splitter', $data);
        });
    }
}
