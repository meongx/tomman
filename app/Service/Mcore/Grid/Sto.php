<?php

namespace App\Service\Mcore\Grid;

use Illuminate\Support\Facades\DB;
use App\Service\Mcore\Helper as Mcore;

class Sto
{
    private static function table()
    {
        return DB::table(Mcore::TYPE_STO);
    }
}
