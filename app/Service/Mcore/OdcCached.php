<?php

namespace App\Service\Mcore;

use App\Service\Cache;
use App\Service\Auth\WorkzoneCached;

class OdcCached
{
    public const PORT_COUNTS = Odc::PORT_COUNTS;
    public const TYPES = Odc::TYPES;

    const PREFIX = 'Mcore.Odc:';
    const TAG_LISTING = self::PREFIX.'List';

    const KEY_COUNT = self::PREFIX.'Count;WorkzonePath=';
    const KEY_BY_WORKZONE = self::PREFIX.'WorkzonePath=%s;Page=%s;Search=%s;Limit=%s';
    const KEY_BY_ID = self::PREFIX.'Id=';
    const KEY_LINKS_SUFFIX = ';Links';
    const KEY_PANEL_SUFFIX = ';Panel';

    public static function tagById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function tagByWorkzoneId($workzoneId)
    {
        return WorkzoneCached::tagById($workzoneId).';'.self::TAG_LISTING;
    }

    public static function keyById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function countByWorkzonePath($path)
    {
        $key = self::KEY_COUNT.$path;

        $dataStore = function () use ($path) {
            return Odc::countByWorkzonePath($path);
        };

        $tagGenerator = function () use ($path) {
            $id = WorkzoneCached::idByPath($path);
            $tag = self::tagByWorkzoneId($id);

            return [$tag];
        };

        return Cache::store($key, $dataStore, $tagGenerator);
    }

    /**
     * flush: WorkzoneCached::flushTagByWorkzoneId
     *
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param string $label
     * @param int $panelcount
     * @param int $portperpanel
     * @param int $type
     * @param float $lat
     * @param float $lng
     * @return int
     * @throws \Throwable when database transaction failed
     */
    public static function create(
        int $user_id,
        int $workzone_id,
        string $label,
        int $panelcount,
        int $portperpanel,
        int $type,
        float $lat,
        float $lng
    ) {
        $id = Odc::create($user_id, $workzone_id, $label, $panelcount, $portperpanel, $type, $lat, $lng);

        WorkzoneCached::flushTagByWorkzoneId($workzone_id, [self::class, 'tagByWorkzoneId']);

        return $id;
    }

    public static function insertHistory($user_id, $odc_id, $operation, array $data)
    {
        Odc::insertHistory($user_id, $odc_id, $operation, $data);

        Cache::del(self::keyById($odc_id));
    }

    public static function getById($id)
    {
        $key = self::keyById($id);

        $dataSource = function () use ($id) {
            return Odc::getById($id);
        };

        $tagGenerator = function () use ($id) {
            return [self::tagById($id)];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    /**
     * flush: keyById, tagByWorkzoneId, tagById
     *
     * @param int $user_id for history/audit
     * @param int $odc_id
     * @param int $workzone_id
     * @param string $label
     * @param int $panelcount
     * @param int $portperpanel
     * @param int $type
     * @param float $lat
     * @param float $lng
     * @throws \Throwable when database transaction failed
     */
    public static function update(
        int $user_id,
        int $odc_id,
        int $workzone_id,
        string $label,
        int $panelcount,
        int $portperpanel,
        int $type,
        float $lat,
        float $lng
    ) {
        Odc::update($user_id, $odc_id, $workzone_id, $label, $panelcount, $portperpanel, $type, $lat, $lng);

        $key = self::keyById($odc_id);
        WorkzoneCached::flushIfKeyExists(
            $key,
            $workzone_id,
            'workzone_id',
            'workzone_path',
            [self::class, 'tagByWorkzoneId'],
            [self::tagById($odc_id)]
        );
    }

    public static function paginateByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $ttl = isset($search) ? 60 * 60 : 0;
        $key = sprintf(self::KEY_BY_WORKZONE, $path, $page, $search, $limit);

        $dataSource = function () use ($path, $page, $search, $limit) {
            return Odc::paginateByWorkzonePath($path, $page, $search, $limit);
        };

        $tagGenerator = function () use ($path) {
            return WorkzoneCached::tagsByWorkzonePath($path, [self::class, 'tagByWorkzoneId']);
        };

        return Cache::store($key, $dataSource, $tagGenerator, $ttl);
    }

    public static function getLinks($id)
    {
        $key = self::keyById($id).self::KEY_LINKS_SUFFIX;

        $dataSource = function () use ($id) {
            return Odc::getLinks($id);
        };

        $tagGenerator = function () use ($id) {
            return [self::tagById($id)];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    /**
     * Not cached due to circular reference and modification
     *
     * @param $odcData
     * @param array $splitterList
     * @param array $linkList
     * @return array
     *
     */
    public static function parsePanelPort($odcData, array $splitterList, array $linkList)
    {
        return Odc::parsePanelPort($odcData, $splitterList, $linkList);
    }
}
