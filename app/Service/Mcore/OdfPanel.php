<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;
use App\Service\Mcore\Helper as Mcore;

class OdfPanel
{
    private static function table()
    {
        return DB::table('mcore.odf_panel');
    }

    private static function auditTable()
    {
        return DB::table('mcore.odf_panel_audit');
    }

    private static function db()
    {
        $q = self::table()->select('odf_panel.*');

        Mcore::odfPanelJoins($q);

        return $q;
    }

    public static function insertHistory($user_id, $panel_id, $operation, array $data)
    {
        self::auditTable()->insert([
            'odf_panel_id' => $panel_id,
            'user_id' => $user_id,
            'timestamp' => DB::raw("NOW() AT TIME ZONE 'utc'"),
            'operation' => $operation,
            'data' => json_encode($data)
        ]);
    }

    /**
     * @param $user_id
     * @param $odf_id
     * @param $label
     * @param $slotcount
     * @param $slotportcount
     * @param $slotvertical
     * @return int
     * @throws \Throwable
     */
    public static function create($user_id, $odf_id, $label, $slotcount, $slotportcount, $slotvertical)
    {
        $id = 0;
        $data = compact('odf_id', 'label', 'slotcount', 'slotportcount', 'slotvertical');
        DB::transaction(function () use ($user_id, $data, &$id) {
            $id = self::table()->insertGetId($data);

            self::insertHistory($user_id, $id, 'insert', $data);
            OdfCached::insertHistory($user_id, $data['odf_id'], 'insert.odfpanel', $data);
        });

        return $id;
    }

    /**
     * @param $user_id
     * @param $panel_id
     * @param $label
     * @param $slotcount
     * @param $slotportcount
     * @param $slotvertical
     * @throws \Throwable
     */
    public static function update($user_id, $panel_id, $label, $slotcount, $slotportcount, $slotvertical)
    {
        $data = compact('label', 'slotcount', 'slotportcount', 'slotvertical');
        DB::transaction(function () use ($user_id, $data, $panel_id) {
            self::table()
                ->where('id', $panel_id)
                ->update($data);

            self::insertHistory($user_id, $panel_id, 'update', $data);
        });
    }

    public static function listByOdf($odf_id)
    {
        return self::table()->where('odf_id', $odf_id)->get();
    }

    public static function getById($id)
    {
        return self::db()->where('odf_panel.id', $id)->first();
    }

    public static function getLinks($id)
    {
        return DB::table('mcore.link')
            ->leftJoin('mcore.odc', function ($join) {
                $join->on('link.dst_type', DB::raw("'odc'"))
                     ->on('link.dst_id', 'odc.id');
            })
            ->leftJoin('auth.workzone AS wz_odc', 'odc.workzone_id', '=', 'wz_odc.id')

            ->leftJoin('mcore.feeder', function ($join) {
                $join->on('link.med_type', DB::raw("'feeder'"))
                     ->on('link.med_id', 'feeder.id');
            })
            ->leftJoin('auth.workzone AS wz_feeder', 'feeder.workzone_id', '=', 'wz_feeder.id')

            ->select(
                'link.*',
                //
                'feeder.id AS feeder_id',
                'feeder.label AS feeder_label',
                'feeder.workzone_id AS feeder_workzone_id',
                'wz_feeder.label AS feeder_workzone_label',
                //
                'odc.id AS odc_id',
                'odc.label AS odc_label',
                'odc.workzone_id AS odc_workzone_id',
                'wz_odc.label AS odc_workzone_label'
            )

            ->where(function ($query) use ($id) {
                $query->where('src_type', 'odf_panel')
                      ->where('src_id', $id);
            })
            ->orWhere(function ($query) use ($id) {
                $query->where('dst_type', 'odf_panel')
                      ->where('dst_id', $id);
            })

            ->orderBy('src_type')
            ->orderBy('src_id')
            ->orderBy('src_val')

            ->get()
        ;
    }

    public static function parsePanelPort($otbData, array $linkList)
    {
        $maxLinksInOnePort = 2; // front and rear
        $totalPort = $otbData->slotcount * $otbData->slotportcount;

        $result = [];

        for ($i = 1; $i <= $totalPort; $i++) {
            $rear = false;
            $front = false;

            $otbPortFront = "$i:front";
            $otbPortRear = "$i:rear";

            $linkFound = [];
            foreach ($linkList as $linkIndex => $link) {
                switch ($link->src_type) {
                    case 'odf_panel':
                        switch ($link->dst_type) {
                            case 'odc':
                                if ($link->src_val === $otbPortRear) {
                                    $rear = $link;

                                    $linkFound[] = $linkIndex;
                                    if (count($linkFound) >= $maxLinksInOnePort) {
                                        break 3;
                                    }
                                }
                                break;
                        }
                        break;
                }
            }
            if (count($linkFound)) {
                foreach ($linkFound as $index) {
                    unset($linkList[$index]);
                }
            }

            $result[$i] = (object)[
                'port' => $i,
                'rear' => $rear,
                'front' => $front
            ];
        }

        return $result;
    }
}
