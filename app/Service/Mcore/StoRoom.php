<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;

class StoRoom
{
    private static function table()
    {
        return DB::table('mcore.sto_room AS room');
    }

    public static function listBySto($id)
    {
        return self::table()->orderBy('label')->where('sto_id', $id)->get();
    }

    public static function getById($id)
    {
        return self::table()->where('id', $id)->first();
    }

    /**
     * @param int $user_id
     * @param int $sto_id
     * @param string $label
     * @return int
     * @throws \Throwable
     */
    public static function create(int $user_id, int $sto_id, string $label)
    {
        $id = 0;
        DB::transaction(function () use ($user_id, $sto_id, $label, &$id) {
            $data = compact('sto_id', 'label');

            StoCached::insertHistory($user_id, $sto_id, 'insert.room', $data);

            $id = self::table()->insertGetId($data);
        });

        return $id;
    }

    /**
     * @param int $user_id
     * @param $room_id
     * @param $label
     * @throws \Throwable
     */
    public static function update(int $user_id, $room_id, $label)
    {
        $roomData = self::getById($room_id);

        DB::transaction(function () use ($user_id, $roomData, $label) {
            $data = compact('label');

            StoCached::insertHistory($user_id, $roomData->sto_id, 'update.room', $data);

            self::table()
                ->where('id', $roomData->id)
                ->update($data);
        });
    }
}
