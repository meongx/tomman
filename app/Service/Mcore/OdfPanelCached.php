<?php

namespace App\Service\Mcore;

use App\Service\Cache;

class OdfPanelCached
{
    const PREFIX = 'Mcore.Sto.Room.OdfPanel:';
    const KEY_BY_ODF = self::PREFIX.'Odf=';
    const KEY_BY_ID = self::PREFIX.'Id=';
    const KEY_LINKS_SUFFIX = ';Links';

    public static function tagById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function keyByOdf($odfId)
    {
        return self::KEY_BY_ODF.$odfId;
    }

    public static function keyById($id)
    {
        return self::KEY_BY_ID.$id;
    }

    public static function insertHistory($user_id, $panel_id, $operation, array $data)
    {
        OdfPanel::insertHistory($user_id, $panel_id, $operation, $data);

        Cache::del(self::keyById($panel_id));
    }

    /**
     * @param $user_id
     * @param $odf_id
     * @param $label
     * @param $slotcount
     * @param $slotportcount
     * @param $slotvertical
     * @return int
     * @throws \Throwable
     */
    public static function create($user_id, $odf_id, $label, $slotcount, $slotportcount, $slotvertical)
    {
        $id = OdfPanel::create($user_id, $odf_id, $label, $slotcount, $slotportcount, $slotvertical);

        Cache::del(self::keyByOdf($odf_id));

        return $id;
    }

    /**
     * @param $user_id
     * @param $panel_id
     * @param $label
     * @param $slotcount
     * @param $slotportcount
     * @param $slotvertical
     * @throws \Throwable
     */
    public static function update($user_id, $panel_id, $label, $slotcount, $slotportcount, $slotvertical)
    {
        OdfPanel::update($user_id, $panel_id, $label, $slotcount, $slotportcount, $slotvertical);

        $data = self::getById($panel_id)[0];
        Cache::flushKeys([self::keyById($panel_id), self::keyByOdf($data->odf_id)]);
    }

    public static function listByOdf($odf_id)
    {
        $key = self::keyByOdf($odf_id);

        $dataSource = function () use ($odf_id) {
            return OdfPanel::listByOdf($odf_id);
        };

        return Cache::store($key, $dataSource);
    }

    public static function getById($id)
    {
        $key = self::keyById($id);

        $dataSource = function () use ($id) {
            return OdfPanel::getById($id);
        };

        return Cache::store($key, $dataSource);
    }

    public static function getLinks($id)
    {
        $key = self::keyById($id).self::KEY_LINKS_SUFFIX;

        $dataSource = function () use ($id) {
            return OdfPanel::getLinks($id);
        };

        $tagGenerator = function () use ($id) {
            return [self::tagById($id)];
        };

        return Cache::store($key, $dataSource, $tagGenerator);
    }

    public static function parsePanelPort($otbData, array $linkList)
    {
        return OdfPanel::parsePanelPort($otbData, $linkList);
    }
}
