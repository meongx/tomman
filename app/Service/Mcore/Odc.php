<?php

namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;
use App\Service\Auth\Workzone;
use App\Service\Mcore\Helper as Mcore;

class Odc
{
    public const PORT_COUNTS = [12, 16, 24];
    public const TYPES = [
        0 => 'ODC-PB',
        1 => 'ODC-C'
    ];

    public const PORT_FRONT = 'front';
    public const PORT_REAR = 'rear';

    private static function table()
    {
        return DB::table('mcore.odc AS odc');
    }

    private static function db()
    {
        return self::table()
            ->leftJoin('auth.workzone AS workzone', 'odc.workzone_id', '=', 'workzone.id')
            ->select(
                'odc.id',
                'odc.label',
                'odc.workzone_id',
                'panelcount',
                'portperpanel',
                'type',
                //
                'workzone.label AS workzone_label',
                'workzone.path AS workzone_path',
                //
                DB::raw('ST_X(odc.coordinate) AS lng'),
                DB::raw('ST_Y(odc.coordinate) AS lat')
            );
    }

    private static function auditTable()
    {
        return DB::table('mcore.odc_audit as audit');
    }

    public static function countByWorkzonePath($path)
    {
        $sql = "
            SELECT
              id,
              label,
              path,
              '/mcore/odc/workzone/' || id AS url,
              CASE 
                WHEN odc.count IS NOT NULL THEN odc.count
                ELSE 0
              END AS count
              
            FROM
              auth.workzone
              
            LEFT JOIN
              (SELECT workzone_id, COUNT(workzone_id) FROM mcore.odc GROUP BY workzone_id) AS odc
              ON odc.workzone_id = id
              
            WHERE
              path <@ ?
              
            ORDER BY
              path
        ";
        $params = [$path];
        $result = DB::select($sql, $params);

        return Workzone::grow($result);
    }

    /**
     * @param int $user_id for history/audit
     * @param int $workzone_id
     * @param string $label
     * @param int $panelcount
     * @param int $portperpanel
     * @param int $type
     * @param float $lat
     * @param float $lng
     * @return int
     * @throws \Throwable when database transaction failed
     */
    public static function create(
        int $user_id,
        int $workzone_id,
        string $label,
        int $panelcount,
        int $portperpanel,
        int $type,
        float $lat,
        float $lng
    ) {
        $id = 0;
        $data = compact('label', 'workzone_id', 'panelcount', 'portperpanel', 'type');

        DB::transaction(function () use (&$id, $data, $user_id, $lat, $lng) {
            $data['coordinate'] = DB::raw("ST_SetSRID(ST_MakePoint({$lng}, {$lat}), 4326)");

            $id = self::table()->insertGetId($data);

            $data['coordinate'] = compact('lat', 'lng');
            self::insertHistory($user_id, $id, 'insert', $data);
        });

        return $id;
    }

    public static function insertHistory($user_id, $odc_id, $operation, array $data)
    {
        $insertData = [
            'odc_id' => $odc_id,
            'user_id' => $user_id,
            'timestamp' => DB::raw("NOW() AT TIME ZONE 'utc'"),
            'operation' => $operation,
            'data' => json_encode($data)
        ];
        self::auditTable()->insert($insertData);
    }

    public static function getById($id)
    {
        //TODO: lastOp

        $data = self::db()->where('odc.id', $id)->first();

        return $data;
    }

    /**
     * @param int $user_id for history/audit
     * @param int $odc_id
     * @param int $workzone_id
     * @param string $label
     * @param int $panelcount
     * @param int $portperpanel
     * @param int $type
     * @param float $lat
     * @param float $lng
     * @throws \Throwable when database transaction failed
     */
    public static function update(
        int $user_id,
        int $odc_id,
        int $workzone_id,
        string $label,
        int $panelcount,
        int $portperpanel,
        int $type,
        float $lat,
        float $lng
    ) {
        $data = compact('label', 'workzone_id', 'panelcount', 'portperpanel', 'type');
        DB::transaction(function () use ($odc_id, $data, $user_id, $lat, $lng) {
            $data['coordinate'] = DB::raw("ST_SetSRID(ST_MakePoint({$lng}, {$lat}), 4326)");

            self::table()->where('id', $odc_id)->update($data);

            $data['coordinate'] = compact('lat', 'lng');
            self::insertHistory($user_id, $odc_id, 'update', $data);
        });
    }

    public static function paginateByWorkzonePath($path, $page = 1, $search = null, $limit = 25)
    {
        $query = self::db()->where('workzone.path', '<@', $path);

        if ($search) {
            $clause = 'odc.label ILIKE ?';
            $term = '%'.str_replace(' ', '%', $search).'%';

            $query->whereRaw($clause, ["%$term%"]);
        }

        $query
            ->leftJoin('mcore.link AS link', function ($join) {
                $join
                    ->on(function ($q) {
                        $q->where('link.src_type', Mcore::TYPE_ODC)
                          ->where('link.src_val', 'LIKE', '%:rear')
                          ->on('link.src_id', 'odc.id')
                        ;
                    })
                    ->orOn(function ($q) {
                        $q->where('link.dst_type', Mcore::TYPE_ODC)
                          ->where('link.dst_val', 'LIKE', '%:rear')
                          ->on('link.dst_id', 'odc.id')
                        ;
                    })
                ;
            })
            ->addSelect(
                DB::raw('(odc.panelcount * odc.portperpanel) AS capacity'),
                DB::raw('COUNT(link.src_id) AS usage')
            )
            ->groupBy('workzone.id', 'odc.id')
            ->orderBy('odc.label')
        ;

        return $query->paginate($limit, ['*'], 'NOTUSED', $page);
    }

    public static function getLinks($id)
    {
        return DB::table('mcore.link')
            /*
             * FEEDER (ODF/OTB - ODC)
             */
            ->leftJoin('mcore.odf_panel', function ($join) {
                $join->on('link.src_type', DB::raw("'".Mcore::TYPE_ODF_PANEL."'"))
                     ->on('link.src_id', 'odf_panel.id');
            })
            ->leftJoin('mcore.odf', 'odf_panel.odf_id', '=', 'odf.id')
            ->leftJoin('mcore.sto_room', 'odf.sto_room_id', '=', 'sto_room.id')
            ->leftJoin('mcore.sto', 'sto_room.sto_id', '=', 'sto.id')
            ->leftJoin('auth.workzone AS wz_sto', 'sto.workzone_id', '=', 'wz_sto.id')

            ->leftJoin('mcore.feeder', function ($join) {
                $join->on('link.med_type', DB::raw("'".Mcore::TYPE_FEEDER."'"))
                     ->on('link.med_id', 'feeder.id');
            })
            ->leftJoin('auth.workzone AS wz_feeder', 'feeder.workzone_id', '=', 'wz_feeder.id')

            /*
             * DISTIRBUSI (ODC - ODP)
             */
            ->leftJoin('mcore.odp', function ($join) {
                $join->on('link.dst_type', DB::raw("'".Mcore::TYPE_ODP."'"))
                     ->on('link.dst_id', 'odp.id');
            })
            ->leftJoin('auth.workzone AS wz_odp', 'odp.workzone_id', '=', 'wz_odp.id')

            ->leftJoin('mcore.distribusi', function ($join) {
                $join->on('link.med_type', DB::raw("'".Mcore::TYPE_DISTRIBUSI."'"))
                     ->on('link.med_id', 'distribusi.id');
            })
            ->leftJoin('auth.workzone AS wz_distribusi', 'distribusi.workzone_id', '=', 'wz_distribusi.id')

            ->select(
                'link.*',
                //
                'odf_panel.id AS odf_panel_id',
                'odf_panel.label AS odf_panel_label',
                'odf.id AS odf_id',
                'odf.label AS odf_label',
                'sto_room.id AS sto_room_id',
                'sto_room.label AS sto_room_label',
                'sto.id AS sto_id',
                'sto.label AS sto_label',
                'sto.workzone_id AS sto_workzone_id',
                'wz_sto.label AS sto_workzone_label',
                //
                'feeder.id AS feeder_id',
                'feeder.label AS feeder_label',
                'feeder.workzone_id AS feeder_workzone_id',
                'wz_feeder.label AS feeder_workzone_label',
                //
                'distribusi.id AS distribusi_id',
                'distribusi.label AS distribusi_label',
                'distribusi.workzone_id AS distribusi_workzone_id',
                'wz_distribusi.label AS distribusi_workzone_label',
                //
                'odp.id AS odp_id',
                'odp.label AS odp_label',
                'odp.workzone_id AS odp_workzone_id',
                'wz_odp.label AS odp_workzone_label'
            )

            ->where(function ($query) use ($id) {
                $query->where('src_type', Mcore::TYPE_ODC)
                      ->where('src_id', $id);
            })
            ->orWhere(function ($query) use ($id) {
                $query->where('dst_type', Mcore::TYPE_ODC)
                      ->where('dst_id', $id);
            })

            ->orderBy('src_type')
            ->orderBy('src_id')
            ->orderBy('src_val')

            ->get()
        ;
    }

    public static function parsePanelPort($odcData, array $splitterList, array $linkList)
    {
        $result = [];

        $maxLinksInOnePort = 2; // front and rear

        for ($i = 1; $i <= $odcData->panelcount; $i++) {
            $panel = [];

            for ($j = 1; $j <= $odcData->portperpanel; $j++) {
                //$n = (($i - 1) * $odcData->portperpanel) + $j;

                $rear = false;
                $front = false;

                $odcPanelPortFront = "$i:$j:".self::PORT_FRONT;
                $odcPanelPortRear = "$i:$j:".self::PORT_REAR;

                $linkFound = [];
                foreach ($linkList as $linkIndex => $link) {
                    switch ($link->src_type) {
                        case Mcore::TYPE_ODF_PANEL:
                            if ($link->dst_type === Mcore::TYPE_ODC) {
                                // ODF/OTB Rear to ODC Rear
                                if ($link->dst_val === $odcPanelPortRear) {
                                    $rear = $link;

                                    $linkFound[] = $linkIndex;
                                    if (count($linkFound) >= $maxLinksInOnePort) {
                                        break 2;
                                    }
                                }
                            }
                            break;

                        case Mcore::TYPE_ODC:
                            switch ($link->dst_type) {
                                // ODC Front to Splitter IN
                                case Mcore::TYPE_ODC_SPLIITER:
                                    if ($link->src_val === $odcPanelPortFront) {
                                        $front = $link;

                                        foreach ($splitterList as $splitter) {
                                            if ($link->dst_type === Mcore::TYPE_ODC_SPLIITER
                                                && $link->dst_id === $splitter->id
                                            ) {
                                                $splitter->ports[$link->dst_val] = $link;
                                                $link->dst_obj = $splitter;

                                                break;
                                            }
                                        }

                                        $linkFound[] = $linkIndex;
                                        if (count($linkFound) >= $maxLinksInOnePort) {
                                            break 3;
                                        }
                                    }
                                    break;

                                // ODC Rear to ODP
                                case Mcore::TYPE_ODP:
                                    if ($link->src_val === $odcPanelPortRear) {
                                        $rear = $link;

                                        $linkFound[] = $linkIndex;
                                        if (count($linkFound) >= $maxLinksInOnePort) {
                                            break 3;
                                        }
                                    }
                                    break;
                            }
                            break;

                        case Mcore::TYPE_ODC_SPLIITER:
                            if ($link->dst_type === Mcore::TYPE_ODC) {
                                // Splitter OUT to ODC Front
                                if ($link->dst_val === $odcPanelPortFront) {
                                    $front = $link;

                                    foreach ($splitterList as $splitter) {
                                        if ($link->src_type === Mcore::TYPE_ODC_SPLIITER
                                            && $link->src_id === $splitter->id
                                        ) {
                                            $splitter->ports[$link->src_val] = $link;
                                            $link->dst_obj = $splitter;

                                            break;
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
                if (count($linkFound)) {
                    foreach ($linkFound as $index) {
                        unset($linkList[$index]);
                    }
                }

                $port = (object)[
                    'port' => $j,
                    'rear' => $rear,
                    'front' => $front
                ];

                $panel[$j] = $port;
            }

            $result[$i] = $panel;
        }

        return $result;
    }
}
