<?php

namespace App\Service;

use Illuminate\Support\Facades\Cache;

abstract class DataCached
{
    const KEY_SUFFIX_TIMESTAMP = '::ts';

    protected static function cacheInstance($tag)
    {
        return Cache::tags($tag);
    }

    protected static function cacheData($cache, $key, $dataSource)
    {
        $value = $cache->rememberForever($key, $dataSource);
        $timestamp = $cache->rememberForever($key.static::KEY_SUFFIX_TIMESTAMP, function () {
            return time();
        });

        return [$value, $timestamp];
    }

    protected static function cacheTimed($cache, $key, $dataSource, $minute)
    {
        $value = $cache->remember($key, $minute, $dataSource);
        $timestamp = $cache->remember($key.static::KEY_SUFFIX_TIMESTAMP, $minute, function () {
            return time();
        });

        return [$value, $timestamp];
    }

    protected static function cacheForget($cache, $key)
    {
        $cache->forget($key);
        $cache->forget($key.static::KEY_SUFFIX_TIMESTAMP);
    }
}
