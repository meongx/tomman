<?php

function _url($path)
{
    $file = public_path($path);
    if (!file_exists($file)) {
        return $path;
    }

    $mtime = filemtime($file);
    return "$path?$mtime";
}

// TODO: refactor into Service
function execWithEnv($command, array $env, $workDir = null)
{
    $descriptors = [
        ['pipe', 'r'], //stdin
        ['pipe', 'w'], //stdout
        ['pipe', 'w']  //stderr
    ];

    $res = proc_open($command, $descriptors, $pipes, $workDir, $env);
    if (!is_resource($res)) {
        return false;
    }

    return stream_get_contents($pipes[1]);
}
