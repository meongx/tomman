#!/usr/bin/env bash

sudo service mysql stop

sudo apt-get --yes --purge remove mysql-server

sudo rm -rf /var/lib/mysql
sudo rm -rf /var/log/mysql
sudo rm -rf /etc/mysql
sudo userdel -r mysql
