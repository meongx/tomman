#!/usr/bin/env bash

sudo apt-get --yes --quiet install php-redis

sudo phpenmod redis
sudo service php*-fpm restart
sudo service nginx restart
