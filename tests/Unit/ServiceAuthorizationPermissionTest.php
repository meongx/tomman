<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Service\Auth\Authorization as Auth;

class ServiceAuthorizationPermissionTest extends TestCase
{
    public function testSingleExactPermissionGranted()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ;
        $userPermission = ['mcore.sto' => Auth::READ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertTrue($result);
    }

    public function testSinglePermissionReadDeniedWrite()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::WRITE;
        $userPermission = ['mcore.sto' => Auth::READ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testSinglePermissionWriteDeniedAudit()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::AUDIT;
        $userPermission = ['mcore.sto' => Auth::WRITE];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testWildcardModuleSinglePermissionGranted()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ;
        $userPermission = ['mcore.*' => Auth::READ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertTrue($result);
    }

    public function testWildcardModuleSinglePermissionReadDeniedWrite()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::WRITE;
        $userPermission = ['mcore.*' => Auth::READ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testWildcardModuleSinglePermissionWriteDeniedAudit()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::AUDIT;
        $userPermission = ['mcore.*' => Auth::WRITE];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testWildcardHigherModuleSinglePermissionGranted()
    {
        $module = 'mcore.sto.odf';
        $requiredPermission = Auth::READ;
        $userPermission = ['mcore.*' => Auth::READ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertTrue($result);
    }

    public function testWildcardHigherModuleSinglePermissionDenied()
    {
        $module = 'mcore.sto.odf';
        $requiredPermission = Auth::WRITE;
        $userPermission = ['mcore.*' => Auth::READ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testSingleModuleCompositePermissionGranted()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ;
        $userPermission = ['mcore.sto' => Auth::READ | Auth::WRITE];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertTrue($result);
    }

    public function testSingleModuleCompositePermissionDenied()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ_AUDIT;
        $userPermission = ['mcore.sto' => Auth::READ | Auth::WRITE];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testWildcardModuleCompositePermissionGranted()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ;
        $userPermission = ['mcore.*' => Auth::READ | Auth::WRITE];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertTrue($result);
    }

    public function testWildcardModuleCompositePermissionDenied()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ_AUDIT;
        $userPermission = ['mcore.*' => Auth::READ | Auth::WRITE];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testSingleExactModuleWithMultiplePermissionGranted()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ;
        $userPermission = [
            'mcore.sto' => Auth::READ,
            'mcore.sto.odf' => Auth::WRITE
        ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertTrue($result);
    }

    public function testSingleExactModuleWithMultiplePermissionDenied()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::WRITE;
        $userPermission = [
            'mcore.sto' => Auth::READ,
            'mcore.sto.odf' => Auth::WRITE
        ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testWildcardModuleMultipleCompositePermissionGranted()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ;
        $userPermission = [
            'workzone.*' => Auth::READ | Auth::READ_AUDIT,
            'mcore.*' => Auth::WRITE
        ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertTrue($result);
    }

    public function testWildcardModuleMultipleCompositePermissionDenied()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::WRITE;
        $userPermission = [
            'workzone.*' => Auth::READ,
            'mcore.*' => Auth::READ | Auth::READ_AUDIT
        ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testParentModuleDenied()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ;
        $userPermission = ['mcore.sto.odf' => Auth::READ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testWildcardParentModuleDenied()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ;
        $userPermission = ['mcore.sto.*' => Auth::READ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testOverridePermissionDenied()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ;
        $userPermission = [
            'mcore.*' => Auth::READ,
            'mcore.sto' => Auth::NONE
        ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertFalse($result);
    }

    public function testOverridePermissionGranted()
    {
        $module = 'mcore.sto';
        $requiredPermission = Auth::READ;
        $userPermission = [
            'mcore.*' => Auth::NONE,
            'mcore.sto' => Auth::READ
        ];

        $result = Auth::hasPermission($module, $requiredPermission, $userPermission);
        $this->assertTrue($result);
    }
}
