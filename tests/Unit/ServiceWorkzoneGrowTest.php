<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Service\Auth\Workzone;

class ServiceWorkzoneGrowTest extends TestCase
{
    public function testGrow()
    {
        $input = [
            (object)[
                'id' => 1,
                'label' => '1',
                'path' => '1'
            ],
            (object)[
                'id' => 4,
                'label' => '1-1',
                'path' => '1.4'
            ],
            (object)[
                'id' => 7,
                'label' => '1-1-1',
                'path' => '1.4.7'
            ],
            (object)[
                'id' => 5,
                'label' => '1-2',
                'path' => '1.5'
            ],
            (object)[
                'id' => 2,
                'label' => '2',
                'path' => '2'
            ],
            (object)[
                'id' => 6,
                'label' => '2-1',
                'path' => '2.6'
            ],
            (object)[
                'id' => 3,
                'label' => '3',
                'path' => '3'
            ]
        ];

        $expectedResult = [
            (object)[
                'id' => 1,
                'label' => '1',
                'path' => '1',

                'children' => [
                    (object)[
                        'id' => 4,
                        'label' => '1-1',
                        'path' => '1.4',

                        'children' => [
                            (object)[
                                'id' => 7,
                                'label' => '1-1-1',
                                'path' => '1.4.7',

                                'children' => []
                            ]
                        ]
                    ],
                    (object)[
                        'id' => 5,
                        'label' => '1-2',
                        'path' => '1.5',

                        'children' => []
                    ]
                ]
            ],
            (object)[
                'id' => 2,
                'label' => '2',
                'path' => '2',

                'children' => [
                    (object)[
                        'id' => 6,
                        'label' => '2-1',
                        'path' => '2.6',

                        'children' => []
                    ]
                ]
            ],
            (object)[
                'id' => 3,
                'label' => '3',
                'path' => '3',

                'children' => []
            ]
        ];

        $result = Workzone::grow($input);
        $this->assertEquals($expectedResult, $result);
    }

    public function testGrowSorted()
    {
        $input = [
            (object)[
                'id' => 1,
                'label' => 'B',
                'path' => '1'
            ],
            (object)[
                'id' => 4,
                'label' => 'B-B',
                'path' => '1.4'
            ],
            (object)[
                'id' => 7,
                'label' => 'B-B-A',
                'path' => '1.4.7'
            ],
            (object)[
                'id' => 5,
                'label' => 'B-A',
                'path' => '1.5'
            ],
            (object)[
                'id' => 2,
                'label' => 'D',
                'path' => '2'
            ],
            (object)[
                'id' => 6,
                'label' => 'D-1',
                'path' => '2.6'
            ],
            (object)[
                'id' => 3,
                'label' => 'A',
                'path' => '3'
            ]
        ];

        $expectedResult = [
            (object)[
                'id' => 3,
                'label' => 'A',
                'path' => '3',

                'children' => []
            ],
            (object)[
                'id' => 1,
                'label' => 'B',
                'path' => '1',

                'children' => [
                    (object)[
                        'id' => 5,
                        'label' => 'B-A',
                        'path' => '1.5',

                        'children' => []
                    ],
                    (object)[
                        'id' => 4,
                        'label' => 'B-B',
                        'path' => '1.4',

                        'children' => [
                            (object)[
                                'id' => 7,
                                'label' => 'B-B-A',
                                'path' => '1.4.7',

                                'children' => []
                            ]
                        ]
                    ]
                ]
            ],
            (object)[
                'id' => 2,
                'label' => 'D',
                'path' => '2',

                'children' => [
                    (object)[
                        'id' => 6,
                        'label' => 'D-1',
                        'path' => '2.6',

                        'children' => []
                    ]
                ]
            ]
        ];

        $result = Workzone::grow($input);
        $this->assertEquals($expectedResult, $result);
    }
}
